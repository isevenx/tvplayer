#ifndef GOTOCLASS_H
#define GOTOCLASS_H

#include <QMainWindow>
#include <QDebug>

namespace Ui {
class GoToClass;
}

class GoToClass : public QMainWindow
{
    Q_OBJECT

public:
    explicit GoToClass(QWidget *parent = 0);
    QString lineValue;
    ~GoToClass();

signals:
    void sendPosition(QString position);

public slots:
    void onClick();

private:
    Ui::GoToClass *ui;

};

#endif // GOTOCLASS_H
