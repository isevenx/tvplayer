#ifndef MODELCREATOR_H
#define MODELCREATOR_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QDebug>
#include <fstream>
#include <QDir>
#include <QFile>

#include "unistd.h"

#include "opencv2/opencv.hpp"
#include "dialog.h"
#include "clipfinder.h"

using namespace std;
using namespace cv;

namespace Ui {
class ModelCreator;
}

class ModelCreator : public QMainWindow
{
    Q_OBJECT

public:
    explicit ModelCreator(QWidget *parent = 0);
    ~ModelCreator();
    void refreshTable();
    Dialog *dialog;
    ClipFinder *clipFinder;
    struct Mark{
        int start;
        int end;
    };
    struct matValues{
        int x;
        int y;
        unsigned char *values;
    };

signals:
    void getModel(QStringList *jpgFiles);

public slots:    
    void addMark(Mark newMark);
    void deleteMark();

    void refreshMarks(QTableWidgetItem* item);
    void createModel();

    void setPath(QString path);

private:
    QString hashMat(Mat frame);
    QString hashHist(Mat frame);
    Mat parseMat(Mat frame);
    Size matSize;
    int histSize;
    bool justAdd;
    QList<Mark> marks;
    Ui::ModelCreator *ui;
};

#endif // MODELCREATOR_H
