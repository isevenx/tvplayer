#include "clipfinder.h"
#include "ui_clipfinder.h"

ClipFinder::ClipFinder(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClipFinder)
{
    ui->setupUi(this);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(false);

    thread = new QThread();
    matcher = new Matcher();
    matcher->moveToThread(thread);
    thread->start();

    QString sourceVideo("/media/rihard/videoDisk/cvWave/02_09/mini_ltv7.mp4");
    QString testVideo("/media/rihard/videoDisk/cvWave/commercial/dzelzcels.mp4");

    matcher->setVideo(sourceVideo, testVideo);
    matcher->setFPS(24.56);
    matcher->setMatSize(6,4);
    matcher->setHistSize(25);

    connect(ui->actionClear,SIGNAL(triggered()),this,SLOT(clearResults()));
    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(findOne()));
    connect(ui->pushButton_2,SIGNAL(clicked()),this,SLOT(findAll()));
    connect(ui->listWidget_2,SIGNAL(itemDoubleClicked(QListWidgetItem*)),this,SLOT(goTo(QListWidgetItem*)));
    connect(ui->listWidget_3,SIGNAL(itemDoubleClicked(QListWidgetItem*)),this,SLOT(goTo(QListWidgetItem*)));
}

ClipFinder::~ClipFinder()
{
    delete ui;
}

void ClipFinder::findOne()
{
    //qDebug() << "Finding one";
    if(ui->listWidget->selectedItems().size() > 0){
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(1);
        ui->progressBar->setTextVisible(true);

        QString it = txtFiles[ui->listWidget->currentRow()];
        matcher->readClip(it);
        while(matcher->ifBusy)
            QThread::msleep(100);

        if((it).endsWith("matx.txt")){
            matcher->setAlg(2);
            matcher->compareParts();
        }
        if((it).endsWith("hist.txt")){
            matcher->setAlg(6);
            matcher->compareParts();
        }
        while(matcher->ifBusy){
            QThread::msleep(100);
        }
        QString sResult, hResult;
        if(!matcher->sMatRes.empty()){
            ui->listWidget_2->clear();
            int pos = (it).lastIndexOf("/");
            QString tString = it;
            tString.remove(0,pos+1);
            ui->listWidget_2->addItem("Clip:  "+tString);
            for(int i = 0;i < matcher->sMatRes.size(); i++){
                sResult.append("     Frame:  "+QString::number(matcher->sMatRes[i].frameNr)\
                               +"  |  "+QString::number(matcher->sMatRes[i].precision,'g',3));
                ui->listWidget_2->addItem(sResult);
                sResult.clear();
            }
            ui->listWidget_2->addItem("========================");
        }
        if(!matcher->histRes.empty()){
            ui->listWidget_3->clear();
            int pos = (it).lastIndexOf("/");
            QString tString = it;
            tString.remove(0,pos+1);
            ui->listWidget_3->addItem("Clip:  "+tString);
            for(int i = 0;i < matcher->histRes.size(); i++){
                hResult.append("     Frame:  "+QString::number(matcher->histRes[i].frameNr)\
                               +"  |  "+QString::number(matcher->histRes[i].precision,'g',3));
                ui->listWidget_3->addItem(hResult);
                hResult.clear();
            }
            ui->listWidget_3->addItem("========================");
        }
        ui->progressBar->setValue(1);
    }
}

void ClipFinder::findAll()
{    
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(txtFiles.size()-1);
    ui->progressBar->setTextVisible(true);

    ui->listWidget_2->clear();
    ui->listWidget_3->clear();

    qDebug() << "";
    qDebug() << "";
    //qDebug() << " ====================== LOAD CLIPS   ====================== ";

    for(QStringList::iterator it = txtFiles.begin(); it < txtFiles.end(); ++it){

        matcher->readClip(*it);
        while(matcher->ifBusy)
            QThread::msleep(100);

        if((*it).endsWith("matx.txt")){
            matcher->setAlg(2);
            matcher->compareParts();
        }
        if((*it).endsWith("hist.txt")){
            matcher->setAlg(6);
            matcher->compareParts();
        }
        while(matcher->ifBusy){
            QThread::msleep(100);
        }
        QString sResult, hResult;
        if(!matcher->sMatRes.empty()){
            int pos = (*it).lastIndexOf("/");
            QString tString = *it;
            tString.remove(0,pos+1);
            ui->listWidget_2->addItem("Clip:  "+tString);
            for(int i = 0;i < matcher->sMatRes.size(); i++){
                sResult.append("     Frame:  "+QString::number(matcher->sMatRes[i].frameNr)\
                               +"  |  "+QString::number(matcher->sMatRes[i].precision,'g',3));
                ui->listWidget_2->addItem(sResult);
                sResult.clear();
            }
            ui->listWidget_2->addItem("========================");
        }
        if(!matcher->histRes.empty()){
            int pos = (*it).lastIndexOf("/");
            QString tString = *it;
            tString.remove(0,pos+1);
            ui->listWidget_3->addItem("Clip:  "+tString);
            for(int i = 0;i < matcher->histRes.size(); i++){
                hResult.append("     Frame:  "+QString::number(matcher->histRes[i].frameNr)\
                               +"  |  "+QString::number(matcher->histRes[i].precision,'g',3));
                ui->listWidget_3->addItem(hResult);
                hResult.clear();
            }
            ui->listWidget_3->addItem("========================");
        }
        ui->progressBar->setValue(it-txtFiles.begin());
        qDebug() << "";
    }    
    //qDebug() << " ====================== CLIPS LOADED ====================== ";

}

void ClipFinder::refreshList()
{

    txtFiles.clear();
    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(false);
    ui->listWidget->clear();    
    QDir dir("model");

    QString path = dir.absolutePath();
    QString name = "*.txt";
    QDirIterator it(path, QStringList() << name, QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()){
        txtFiles << it.next();
    }
    txtFiles.sort(Qt::CaseInsensitive);
    for(int i = 0; i < txtFiles.size(); i++){
        int pos = txtFiles[i].lastIndexOf("/");
        QString tString = txtFiles[i];
        tString.remove(0,pos+1);
        ui->listWidget->addItem(tString);
    }
}

void ClipFinder::clearResults()
{
    ui->listWidget_2->clear();
    ui->listWidget_3->clear();
}

void ClipFinder::goTo(QListWidgetItem* item)
{
    QString itemText = item->text();
    itemText.remove(0,13);
    int palka = itemText.indexOf("|");
    itemText.remove(palka-2,itemText.length());
    QRegExp re("\\d*");
    if(re.exactMatch(itemText)){
        emit sendGoTo(itemText);
    }
}
