#ifndef DIALOG_H
#define DIALOG_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSpinBox>
#include <QDebug>

namespace Ui {
class Dialog;
}

class Dialog : public QMainWindow
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    struct Mark{
        int start;
        int end;
    };

signals:
    void sendMark(Mark mark);

public slots:
    void showAddMark();
    void showEditMark();
    void buttonOK();
    void clearLines();

private:
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QSpinBox *spinBox;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    Ui::Dialog *ui;
};

#endif // DIALOG_H
