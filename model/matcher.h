#ifndef Matcher_H
#define Matcher_H

#include <QObject>
#include "opencv2/opencv.hpp"
#include <QThread>
#include <QDebug>
#include <QTime>
#include <vector>
#include <QFileInfo>
#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QPair>
#include <iostream> // library that contain basic input/output functions
#include <fstream>  // library that contains file input/output functions

using namespace cv;
using namespace std;

class Matcher : public QObject
{
    Q_OBJECT
public:
    explicit Matcher(QObject *parent = 0);
    Matcher(QString sVideo, QString tVideo, bool relearn);
    ~Matcher();

    void setAlg(int algnr);
    void setVideo(QString sourceVideo, QString testVideo);
    void setFrameCount(int fCount);
    void setMatSize(int height, int width);
    void setHistSize(int size);
    void setFPS(float fps);
    void showAlg();
    void showFrame(int frameNr);

    void loadModel(bool relearn);
    void loadPart();

    void writeModels();
    void readModels();
    void loadParts();

    void compareParts();
    void compareFrame();

    Mat parseMat(Mat frame);

    QString path;
    bool ifBusy;
    struct Results{        
        int frameNr;
        double precision;
    };

    QList<Results> sMatRes;
    QList<Results> histRes;
    QList<int> borders;

public slots:
    void readAllModels();
    void readClip(QString clipName);

private:
    int histSize;
    Size matSize;
    int algNr;
    float delta;
    float FPS;
    int frameCount, ddd;

    QString sourceVideo;
    QString testVideo;

    VideoCapture cap;
    VideoCapture cap2;

    QFileInfo sourceModel;
    FileStorage fs;

    vector<int> history;
    vector<int> part;

    vector<Mat> fsMatrix;
    vector<Mat> partMatrix;

    struct matValues{
        int x;
        int y;
        unsigned char *values;
    };
    QList<matValues> txtMatrix;
    QList<matValues> partMatrix_2;

    QList<QList<long> > txtHist;
    QList<QList<long> > partHist;

    //vector<Mat> vecMatrix;

    QList<int> pxValues;

    int hashMat(Mat frame);
    double hashOtsu(Mat src);
    QPair<long, int> hashHist(QList<long> hist_1, QList<long> hist_2);
    float compareMat(Mat frame_1, Mat frame_2);
    float compareMat(matValues matV1, matValues matV2);
    float sortCompare(matValues matV1, matValues matV2);

    double getPSNR(Mat frame_1, Mat frame_2);
    Scalar getMSSIM(Mat frame_1, Mat frame_2);

    Mat createHist(Mat src);
    double compHist(Mat src_base, Mat src_test1, int method);

signals:

public slots:
};

#endif // Matcher_H
