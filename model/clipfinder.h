#ifndef CLIPFINDER_H
#define CLIPFINDER_H

#include <QMainWindow>
#include <QDebug>
#include <QThread>
#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QListWidgetItem>

#include "matcher.h"

namespace Ui {
class ClipFinder;
}

class ClipFinder : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClipFinder(QWidget *parent = 0);
    ~ClipFinder();
    QThread *thread;
    Matcher *matcher;
    QString path;
    QStringList txtFiles;
signals:
    void sendGoTo(QString position);

public slots:
    void findOne();
    void findAll();
    void refreshList();
    void clearResults();
    void goTo(QListWidgetItem* item);

private:
    Ui::ClipFinder *ui;
};

#endif // CLIPFINDER_H
