#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->resize(0,0);
    label = new QLabel();
    lineEdit = new QLineEdit();
    lineEdit_2 = new QLineEdit();
    pushButton = new QPushButton("OK");
    pushButton_2 = new QPushButton("Cancel");
    spinBox = new QSpinBox();

    QGridLayout *gBox = new QGridLayout();
    label->setText("Add mark: ");
    gBox->addWidget(label,0,0);
    gBox->addWidget(lineEdit,0,1);
    gBox->addWidget(lineEdit_2,0,2);
    gBox->addWidget(pushButton_2,1,1);
    gBox->addWidget(pushButton,1,2);
    ui->centralwidget->setLayout(gBox);

    connect(pushButton_2,SIGNAL(clicked()),this,SLOT(close()));
    connect(pushButton_2,SIGNAL(clicked()),this,SLOT(clearLines()));
    connect(pushButton,SIGNAL(clicked()),this,SLOT(buttonOK()));
    connect(pushButton,SIGNAL(clicked()),this,SLOT(clearLines()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::showAddMark()
{
//    QGridLayout *gBox = new QGridLayout();
//    label->setText("Add mark: ");
//    gBox->addWidget(label,0,0);
//    gBox->addWidget(lineEdit,0,1);
//    gBox->addWidget(lineEdit_2,0,2);
//    gBox->addWidget(pushButton_2,1,1);
//    gBox->addWidget(pushButton,1,2);
//    ui->centralwidget->setLayout(gBox);
    this->show();
}

void Dialog::showEditMark()
{

}

void Dialog::buttonOK()
{
    Mark mark;

    QRegExp re("\\d*");
    if(re.exactMatch(lineEdit->text()) && re.exactMatch(lineEdit_2->text())){
        mark.start = lineEdit->text().toInt();
        mark.end = lineEdit_2->text().toInt();
    }
    else{
        qDebug() << "Incorrect numbers!";
        return;
    }
    sendMark(mark);
    this->close();
}

void Dialog::clearLines()
{
    lineEdit->clear();
    lineEdit_2->clear();
}
