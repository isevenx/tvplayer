#include "modelcreator.h"
#include "ui_modelcreator.h"

ModelCreator::ModelCreator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ModelCreator)
{
    ui->setupUi(this);
    dialog = new Dialog();
    clipFinder = new ClipFinder();

    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(2);
    QStringList headers;
    headers <<"Start" << "End";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

    connect(dialog,SIGNAL(sendMark(Mark)),this,SLOT(addMark(Mark)));
    connect(ui->tableWidget,SIGNAL(itemChanged(QTableWidgetItem*)),this,SLOT(refreshMarks(QTableWidgetItem*)));

    connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(createModel()));
    connect(ui->pushButton_2,SIGNAL(clicked()),dialog,SLOT(showAddMark()));
    connect(ui->pushButton_3,SIGNAL(clicked()),this,SLOT(deleteMark()));

    histSize = 25;
    matSize = Size(6,4);
}

ModelCreator::~ModelCreator()
{
    delete ui;
    delete dialog;
    delete clipFinder;
}

void ModelCreator::refreshTable()
{
    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(marks.size());
    ui->tableWidget->setColumnCount(2);
    QStringList headers;
    headers << "Start" << "End";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    int it;
    justAdd = true;
    for(it = 0; it <marks.size(); ++it){
        ui->tableWidget->setItem(it,0,new QTableWidgetItem(QString::number(marks.at(it).start)));
        ui->tableWidget->setItem(it,1,new QTableWidgetItem(QString::number(marks.at(it).end)));
    }
    justAdd = false;
}

void ModelCreator::refreshMarks(QTableWidgetItem* item)
{
    if(justAdd)
        return;
    Mark mark;
    mark.start = marks.at(item->row()).start;
    mark.end = marks.at(item->row()).end;

    if(item->column() == 0){
        QRegExp re("\\d*");
        if(re.exactMatch(item->text())){
            mark.start = item->text().toInt();
        }
    }
    if(item->column() == 1){
        QRegExp re("\\d*");
        if(re.exactMatch(item->text())){
            mark.end = item->text().toInt();
        }
    }
    marks.removeAt(item->row());
    marks.insert(item->row()-1,mark);
    refreshTable();
}

void ModelCreator::addMark(Mark newMark)
{
    marks.push_back(newMark);
    refreshTable();
}

void ModelCreator::deleteMark()
{
    QList<QTableWidgetItem*> selectedItems = ui->tableWidget->selectedItems();
    for(QList<QTableWidgetItem*>::iterator it = selectedItems.end()-1; it > selectedItems.begin(); it-=2){
        marks.removeAt((*it)->row());
    }
    refreshTable();
}

void ModelCreator::createModel()
{
    QDir dir("model");
    if(!dir.exists()){
        dir.mkdir(dir.absolutePath());
    }
    QStringList jpgFiles;
    getModel(&jpgFiles);

    int nMat = 0;
    int nHist = 0;
    string fileName;
    fstream fout1,fout2;

    for(QList<Mark>::iterator it = marks.begin(); it < marks.end(); ++it){
        if((*it).start < (*it).end){

            fileName = QString("model/"+QString::number(it-marks.begin()+nMat)+"_matx.txt").toStdString();
            while(access( fileName.c_str(), F_OK ) != -1){
                nMat++;
                fileName = QString("model/"+QString::number(it-marks.begin()+nMat)+"_matx.txt").toStdString();
            }
            fout1.open(fileName.c_str(),std::fstream::out | std::fstream::trunc);

            fileName = QString("model/"+QString::number(it-marks.begin()+nMat)+"_hist.txt").toStdString();
            while(access( fileName.c_str(), F_OK ) != -1){
                nHist++;
                fileName = QString("model/"+QString::number(it-marks.begin()+nMat)+"_hist.txt").toStdString();
            }
            fout2.open(fileName.c_str(),std::fstream::out | std::fstream::trunc);

            if(!fout1.is_open() || !fout2.is_open()){
                qDebug() << "Cant open files! (Have you created model folder?)";
                return;
            }
            for(int i = (*it).start; i <= (*it).end; ++i){
                Mat frame = cv::imread(jpgFiles.at(i).toStdString());
                if(frame.empty()){
                    qDebug() << "Empty frame: " << i <<"!";
                    continue;
                }
                fout1 << i-(*it).start << ":" << hashMat(frame.clone()).toStdString().c_str() << std::endl;
                fout2 << i-(*it).start << ":" << hashHist(frame.clone()).toStdString().c_str() << std::endl;
            }
            fout1.close();
            fout2.close();
        }
    }
    this->close();
}

Mat ModelCreator::parseMat(Mat frame)
{
    Mat grayFrame (frame.size(), CV_8UC1);
    cv::cvtColor(frame,grayFrame,CV_BGR2GRAY);
    cv::resize(grayFrame,grayFrame,matSize);
    return grayFrame;
}

QString ModelCreator::hashMat(Mat frame)
{
    Mat tMatrix = parseMat(frame);
    string scValues;
    matValues matV;
    matV.x = tMatrix.cols;
    matV.y = tMatrix.rows;
    matV.values = new unsigned char[matV.x*matV.y];
    int j = -1;
    for(int y = 0;y < tMatrix.rows; y++){
        for(int x = 0; x < tMatrix.cols; x++){
            ++j;
            Scalar tValue = tMatrix.at<uchar>(Point(x, y));
            matV.values[j] = tValue.val[0];
            scValues.append(QString::number(tValue.val[0]).toStdString());
            scValues.append(",");
        }
    }
    scValues.erase(scValues.length()-1);

    return QString(QString::number(tMatrix.cols)+ "," +QString::number(tMatrix.rows) + "{"\
                   + scValues.c_str() + "}");
}

QString ModelCreator::hashHist(Mat frame)
{
    string histValues;
    QList<long> tempHist;
    long tempValue = 0;
    Mat src;
    cvtColor(frame,src,CV_BGR2GRAY);
    uchar * src_p, * src_end;
    long histogram[256] = {0};
    for( src_p = src.data, src_end = src.data + src.rows * src.cols;
         src_p != src_end; ++src_p)
    {
        histogram[*src_p]++;
    }
    for(int i = 0; i < 256; i++){
        tempValue += histogram[i];
        if(i%int(255/histSize) == 0 && i<250 && i != 0){
            tempHist.push_back(tempValue);
            histValues.append(QString::number(tempValue).toStdString());
            histValues.append(",");
            tempValue = 0;
        }
    }
    histValues.append(QString::number(tempValue).toStdString());

    return QString(QString::number(histSize)+ "{" +histValues.c_str()+ "}");
}

void ModelCreator::setPath(QString path)
{
    clipFinder->path = path;
    clipFinder->matcher->path = path;
    clipFinder->matcher->readAllModels();
}
