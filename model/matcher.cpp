#include "matcher.h"

///=============== Constructors and Destructor ==========================

Matcher::Matcher(QObject *parent) : QObject(parent)
{
    path = "";
    algNr = -1;
    setMatSize(6,4);
    setHistSize(25);
    ddd = 0;
    FPS = 0;
    frameCount = -1;
    ifBusy = false;
}

Matcher::Matcher(QString sVideo, QString tVideo, bool relearn)
{
    setMatSize(7,4);
    ddd = 0;
    FPS = 0;
    //frameCount = 17400;

    setVideo(sVideo,tVideo);
    loadParts();

    setAlg(0);
    if(history.empty())
        loadModel(relearn);
    compareParts();
}

Matcher::~Matcher()
{
    txtMatrix.clear();
    partMatrix_2.clear();
}

///=============== Set parameters =======================================

void Matcher::setAlg(int algnr)
{
    if(algnr < 0 || algnr > 6){
        qDebug() << "Wrong algorithm === setting alg to 0";
        algNr = 0;
    }
    else
        algNr = algnr;
}

void Matcher::setVideo(QString sourceVideo, QString testVideo)
{
    this->sourceVideo = sourceVideo;
    this->testVideo = testVideo;
}

void Matcher::setMatSize(int height, int width)
{
    matSize = Size(height, width);
}

void Matcher::setHistSize(int size)
{
    histSize = size;
}

void Matcher::setFPS(float fps)
{
    FPS = fps;
}

void Matcher::setFrameCount(int fCount)
{
    frameCount = fCount;
}

void Matcher::showAlg()
{
    qDebug() << "";
    qDebug() << "Algorithm number: " << algNr;
    if(algNr == 0){
        qDebug() << "This algorithm is comparing avg pixel value on full matrix";
    }
    if(algNr == 1){
        qDebug() << "This algorithm is comparing grayscale matrix";
        qDebug() << "with given size into/from yml file using fileStorage(opencv)";
    }
    if(algNr == 2){
        qDebug() << "This algorithm is comparing grayscale matrix with given size";
    }
    if(algNr == 3){
        qDebug() << "This algorithm is comparing avg pixel value";
        qDebug() << "on full matrix using PSNR algorithm";
    }
    if(algNr == 4){
        qDebug() << "This algorithm is comparing avg pixel value";
        qDebug() << "on full matrix using SSIM algorithm";
    }
    if(algNr == 5){
        qDebug() << "This algorithm is comparing histograms";
        qDebug() << "Using opencv comparing functions";
    }
    if(algNr == 6){
        qDebug() << "This algorithm is comparing grayscale histograms";
        qDebug() << "Dividing them on N parts";
    }
}

void Matcher::showFrame(int frameNr)
{
    if(!cap.isOpened())
        cap.open(sourceVideo.toStdString());

    Mat frame;
    static int lastFrame = 0;
    while(lastFrame < frameNr){
        cap >> frame;
        if (frame.empty()) break;
        lastFrame++;
    }
    if(frame.empty()){
        qDebug() << " EMPTY FRAME";
        return;
    }
    imshow("edges",frame);
    waitKey(300000);
}

///=============== Load model ===========================================

void Matcher::loadModel(bool relearn)
{
    QTime bench,bencHash;
    int hashFtime = 0;
    cap.open(sourceVideo.toStdString().c_str());
    if(!cap.isOpened()){
        qDebug() << "Incorrect source video ===== exiting";
        return;
    }
    if(FPS == 0)
        setFPS(cap.get(CV_CAP_PROP_FPS));

    if(algNr == 0){
        sourceModel.setFile(QString(sourceVideo+".model"));
    }
    if(algNr == 1){
        sourceModel.setFile(QString(sourceVideo+"_matrix.yml"));
    }
    if(algNr == 2){
        sourceModel.setFile(QString(sourceVideo+"_matrix.txt"));
    }
    if(algNr == 6){
        sourceModel.setFile(QString(sourceVideo+"_histogram.txt"));
    }

    bench.start();
    qDebug() << "";
    qDebug() << " ====================== MODEL LOADING ===================== ";
    if(!sourceModel.exists() || relearn){

        fstream fout;
        if(algNr == 0 || algNr == 2 || algNr == 6){
            fout.open(sourceModel.absoluteFilePath().toStdString().c_str(),std::fstream::out | std::fstream::trunc);
            if(!fout.is_open()){
                qDebug() << "Cant open source model ===== exiting";
                return;
            }
        }
        if(algNr == 1){
            fs.open(sourceModel.absoluteFilePath().toStdString(), FileStorage::WRITE);
            if(!fs.isOpened()){
                qDebug() << "Cant open source model ===== exiting";
                return;
            }
        }

        ddd = 0;
        while(ddd != frameCount)
        {
            bencHash.start();
            Mat frame;
            cap >> frame;
            if(frame.empty()) break;
            int hash;
            ddd++;

            if(algNr == 0){ /// Pirma algoritma ieraksts
                hash = hashMat(frame);
                history.push_back((hash + (history.empty()?0:history.back())) / (history.empty()?1:2) );
                fout << history.back() << std::endl;
            }
            if(algNr == 1){ /// Otra  algoritma ieraksts
                Mat tMatrix = parseMat(frame);
                fsMatrix.push_back(tMatrix);
                fs << QString("matrix_").append(QString::number(ddd)).toStdString() << tMatrix;
            }
            if(algNr == 2){ /// Otra+ algoritma ieraksts
                Mat tMatrix = parseMat(frame);

                string scValues;
                matValues matV;

                matV.x = tMatrix.cols;
                matV.y = tMatrix.rows;
                matV.values = new unsigned char[matV.x*matV.y];

                int j = -1;

                for(int y = 0;y < tMatrix.rows; y++){
                    for(int x = 0; x < tMatrix.cols; x++){
                        ++j;
                        Scalar tValue = tMatrix.at<uchar>(Point(x, y));
                        matV.values[j] = tValue.val[0];
                        scValues.append(QString::number(tValue.val[0]).toStdString());
                        scValues.append(",");
                    }
                }
                txtMatrix.push_back(matV);
                scValues.erase(scValues.length()-1);
                fout << tMatrix.cols << "," << tMatrix.rows << "{"\
                     << scValues << "}" << std::endl;

            }
            if(algNr == 6){ /// Tresa algoritma ieraksts

                string histValues;
                QList<long> tempHist;
                long tempValue = 0;

                Mat src;
                cvtColor(frame,src,CV_BGR2GRAY);
                uchar * src_p, * src_end;
                long histogram[256] = {0};
                for( src_p = src.data,
                     src_end = src.data + src.rows * src.cols;
                     src_p != src_end; ++src_p)
                {
                    histogram[*src_p]++;
                }

                for(int i = 0; i < 256; i++){
                    tempValue += histogram[i];
                    if(i%int(255/histSize) == 0 && i<250 && i != 0){
                        tempHist.push_back(tempValue);
                        histValues.append(QString::number(tempValue).toStdString());
                        histValues.append(",");
                        tempValue = 0;
                    }
                }

                histValues.append(QString::number(tempValue).toStdString());
                tempHist.push_back(tempValue);
                txtHist.push_back(tempHist);
                fout << histSize << "{" << histValues << "}" << std::endl;

            }

            if(ddd%int(FPS*60) == 0) qDebug() << " minutes done: " << floor((double)ddd/FPS/60+1) << "=== hashing:" <<hashFtime/ddd << "ms";
            hashFtime += bencHash.elapsed();

        }
        fs.release();
        fout.close();
    }
    else{

        if(algNr == 0){ /// Pirma algoritma ielasisana
            ifstream fin(sourceModel.absoluteFilePath().toStdString().c_str() );
            if(!fin.is_open()){
                qDebug() << "Cant open source model ===== exiting";
                return;
            }
            int ti ;
            while(true){
                fin >> ti;
              //  qDebug() << ti;
                history.push_back(ti);
                if(fin.eof()) break;
            }
        }
        if(algNr == 1){ /// Otra  algoritma ielasisana
            fs.open(sourceModel.absoluteFilePath().toStdString(), FileStorage::READ);
            ifstream fin(sourceModel.absoluteFilePath().toStdString().c_str() );
            if(!fin.is_open() || !fs.isOpened()){
                qDebug() << "Cant open source model ===== exiting";
                return;
            }
            string findMatrix;
            while(true){
                fin >> findMatrix;
                if(findMatrix.compare(0,7,"matrix_") == 0){
                    Mat matFromFile;
                    findMatrix.erase(findMatrix.length()-1);
                    fs[findMatrix] >> matFromFile;
                    if(!matFromFile.empty())
                        fsMatrix.push_back(matFromFile);
                }
                if(fin.eof()) break;
            }
            fs.release();
        }
        if(algNr == 2){ /// Otra+ algoritma ielasisana
            ifstream fin(sourceModel.absoluteFilePath().toStdString().c_str() );
            if(!fin.is_open()){
                qDebug() << "Cant open source model ===== exiting";
                return;
            }
            matValues matV;

            int curPos1,curPos2;
            while(true){
                string scValues;
                fin >> scValues;
                if(scValues.empty() || fin.eof()) break;
                curPos1 = scValues.find(",");
                curPos2 = scValues.find("{");
                matV.x = atoi(scValues.substr(0,curPos1).c_str());
                matV.y = atoi(scValues.substr(curPos1+1,curPos2-1).c_str());
                matV.values = new unsigned char[matV.x*matV.y];
                scValues.erase(0,curPos2+1);

                int j = -1;
                while(curPos1!= -1){
                    ++j;
                    curPos1 = scValues.find(",");
                    if(curPos1 != (int)std::string::npos){
                        matV.values[j] = atoi(scValues.substr(0,curPos1).c_str());
                        scValues.erase(0,curPos1+1);
                    }
                }

                curPos2 = scValues.find("}");
                scValues.erase(curPos2,curPos2+1);
                matV.values[matV.x*matV.y-1] = atoi(scValues.c_str());
                txtMatrix.push_back(matV);
            }

        }
        if(algNr == 6){ /// Tresa algoritma ielasisana

            ifstream fin(sourceModel.absoluteFilePath().toStdString().c_str() );
            if(!fin.is_open()){
                qDebug() << "Cant open source model ===== exiting";
                return;
            }
            while(true){
                string histValues;
                int curPos1, curPos2;
                fin >> histValues;
                if(histValues.empty() || fin.eof()) break;
                curPos1 = histValues.find(",");
                curPos2 = histValues.find("{");
                const int size = atoi(histValues.substr(0,curPos2).c_str());
                histValues.erase(0,curPos2+1);

                if(size != histSize){
                    qDebug() << "Different histogram size ===== exiting";
                    return;
                    //setHistSize(size);
                }

                QList<long> hist;
                int j = -1;
                while(curPos1!= -1){
                    j++;
                    curPos1 = histValues.find(",");
                    if(curPos1 != (int)std::string::npos){
                        hist.append(atoi(histValues.substr(0,curPos1).c_str()));
                        histValues.erase(0,curPos1+1);
                    }
                }
                curPos2 = histValues.find("}");
                histValues.erase(curPos2,curPos2+1);
                hist.append(atoi(histValues.c_str()));
                txtHist.push_back(hist);
            }
        }
    }

    qDebug() << " ====================== MODEL LOADED ====================== " << bench.elapsed()<<"ms";
}

void Matcher::writeModels()
{
    QTime bench, bencHash;
    int hashFtime = 0;
    cap.open(sourceVideo.toStdString().c_str());
    if(!cap.isOpened()){
        qDebug() << "Incorrect source video ===== exiting";
        return;
    }
    if(FPS == 0)
        setFPS(cap.get(CV_CAP_PROP_FPS));

    bench.start();
    qDebug() << "";
    qDebug() << " ====================== MODEL LOADING ===================== ";

    fstream fout,fout2,fout3;

    fout.open(QString(sourceVideo+".model").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
    fout2.open(QString(sourceVideo+"_matrix.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
    fout3.open(QString(sourceVideo+"_histogram.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);

    if(!fout.is_open() || !fout2.is_open() || !fout3.is_open()){
        qDebug() << "Cant open source models ===== exiting";
        return;
    }

    ddd = 0;

    while(ddd != frameCount)
    {
        bencHash.start();
        Mat frame;
        cap >> frame;
        if(frame.empty()) break;
        int hash;
        ddd++;
        if(ddd%int(FPS*60) == 0) qDebug() << " minutes done:" << floor((double)ddd/FPS/60+1) << "=== hashing:" <<hashFtime/ddd << "ms";

        //  Pirmais algoritms
        hash = hashMat(frame.clone());
        history.push_back((hash + (history.empty()?0:history.back())) / (history.empty()?1:2) );
        fout << history.back() << std::endl;

        // Otrais algoritms
        Mat tMatrix = parseMat(frame.clone());
        string scValues;
        matValues matV;
        matV.x = tMatrix.cols;
        matV.y = tMatrix.rows;
        matV.values = new unsigned char[matV.x*matV.y];
        int j = -1;
        for(int y = 0;y < tMatrix.rows; y++){
            for(int x = 0; x < tMatrix.cols; x++){
                ++j;
                Scalar tValue = tMatrix.at<uchar>(Point(x, y));
                matV.values[j] = tValue.val[0];
                scValues.append(QString::number(tValue.val[0]).toStdString());
                scValues.append(",");
            }
        }
        txtMatrix.push_back(matV);
        scValues.erase(scValues.length()-1);
        fout2 << tMatrix.cols << "," << tMatrix.rows << "{"\
             << scValues << "}" << std::endl;

        // Tresais algoritms
        string histValues;
        QList<long> tempHist;
        long tempValue = 0;
        Mat src;
        cvtColor(frame,src,CV_BGR2GRAY);
        uchar * src_p, * src_end;
        long histogram[256] = {0};
        for( src_p = src.data, src_end = src.data + src.rows * src.cols;
             src_p != src_end; ++src_p)
        {
            histogram[*src_p]++;
        }
        for(int i = 0; i < 256; i++){
            tempValue += histogram[i];
            if(i%int(255/histSize) == 0 && i<250 && i != 0){
                tempHist.push_back(tempValue);
                histValues.append(QString::number(tempValue).toStdString());
                histValues.append(",");
                tempValue = 0;
            }
        }
        histValues.append(QString::number(tempValue).toStdString());
        tempHist.push_back(tempValue);
        txtHist.push_back(tempHist);
        fout3 << histSize << "{" << histValues << "}" << std::endl;

        hashFtime += bencHash.elapsed();
    }
    fout.close();
    fout2.close();
    fout3.close();
    qDebug() << " ====================== MODEL LOADED ====================== " << bench.elapsed()<<"ms";
}

        ///======== Otwdvr ==============================================

void Matcher::readModels()
{
    ifstream fin;
    fin.open(QString(sourceVideo+"sMatrix.txt").toStdString().c_str() );
    if(!fin.is_open())
        qDebug() << "Cant open source model Nr 2 =====";
    else{        
        matValues matV;

        int curPos1,curPos2;
        while(true){
            string scValues;
            fin >> scValues;
            if(scValues.empty() || fin.eof()) break;
            curPos1 = scValues.find(":");
            scValues.erase(0,curPos1+1);
            curPos1 = scValues.find(",");
            curPos2 = scValues.find("{");
            matV.x = atoi(scValues.substr(0,curPos1).c_str());
            matV.y = atoi(scValues.substr(curPos1+1,curPos2-1).c_str());
            matV.values = new unsigned char[matV.x*matV.y];
            scValues.erase(0,curPos2+1);

            int j = -1;
            while(curPos1!= -1){
                ++j;
                curPos1 = scValues.find(",");
                if(curPos1 != (int)std::string::npos){
                    matV.values[j] = atoi(scValues.substr(0,curPos1).c_str());
                    scValues.erase(0,curPos1+1);
                }
            }
            curPos2 = scValues.find("}");
            scValues.erase(curPos2,curPos2+1);
            matV.values[matV.x*matV.y-1] = atoi(scValues.c_str());
            txtMatrix.push_back(matV);
        }
        fin.close();        
    }
    fin.open(QString(sourceVideo+"histogram.txt").toStdString().c_str());
    if(!fin.is_open())
        qDebug() << "Cant open source model Nr 3 =====";
    else{
        while(true){
            string histValues;
            int curPos1, curPos2;
            fin >> histValues;
            if(histValues.empty() || fin.eof()) break;
            curPos1 = histValues.find(":");
            histValues.erase(0,curPos1+1);
            curPos1 = histValues.find(",");
            curPos2 = histValues.find("{");
            const int size = atoi(histValues.substr(0,curPos2).c_str());
            histValues.erase(0,curPos2+1);

            if(size != histSize){
                qDebug() << "Different histogram size ===== exiting";
                return;
                //setHistSize(size);
            }

            QList<long> hist;
            int j = -1;
            while(curPos1!= -1){
                j++;
                curPos1 = histValues.find(",");
                if(curPos1 != (int)std::string::npos){
                    hist.append(atoi(histValues.substr(0,curPos1).c_str()));
                    histValues.erase(0,curPos1+1);
                }
            }
            curPos2 = histValues.find("}");
            histValues.erase(curPos2,curPos2+1);
            hist.append(atoi(histValues.c_str()));
            txtHist.push_back(hist);
        }
    }    
}

void Matcher::readAllModels()
{
    if(path == ""){
        qDebug() << "Empty path!";
        return;
    }
    ifBusy = true;
    txtMatrix.clear();
    txtHist.clear();

    QTime bench;
    bench.start();
    qDebug() << "";
    qDebug() << " ====================== LOAD MODELS  ====================== ";

    QString name = "*.txt";
    QStringList txtFiles;
    QDirIterator it(path, QDir::Dirs | QDir::NoDotAndDotDot);
    while (it.hasNext()){
        QString hours = it.next();
        QDirIterator it2(hours, QDir::Dirs | QDir::NoDotAndDotDot);
        while(it2.hasNext()){
            QDirIterator it3(it2.next(), QStringList() << name, QDir::Files);
            while (it3.hasNext()){
                txtFiles << it3.next();
            }
        }
    }
    txtFiles.sort(Qt::CaseInsensitive);
    for(int i = 0; i < txtFiles.size(); i+=2){
        QString tString = txtFiles[i];
        tString.chop(13);
        this->setVideo(tString,"");
        this->readModels();
    }
    qDebug() << " ====================== MODEL LOADED ====================== " << bench.elapsed()<<"ms";

    setAlg(6);
    compareFrame();

    ifBusy = false;
}

///=============== Load part ============================================

void Matcher::loadPart()
{
    QTime bench;
    QTime bencHash;
    QTime frameRead;
    long hashtime = 0, ftime = 0;

    Mat frame;
    cap2.open(testVideo.toStdString().c_str());
    if(!cap2.isOpened()){
        qDebug() << "Incorrect test video ===== exiting";
        return;
    }

    if(FPS == 0)
        setFPS(cap2.get(CV_CAP_PROP_FPS));

    if(algNr == 0 && !part.empty())
        part.clear();
    if(algNr == 1 && !partMatrix.empty())
        partMatrix.clear();
    if(algNr == 2 && !partMatrix_2.empty()){
        partMatrix_2.clear();
    }
    if(algNr == 6 && !partHist.empty()){
        partHist.clear();
    }

    bench.start();
    qDebug() << "";
    qDebug() << " ====================== PART LOADING ====================== ";
    ddd = 0;

    for(;;){

        frameRead.start();
        cap2 >> frame;
        if(!frame.data)
            break;
        ftime += frameRead.elapsed();
        int hash = 0;
        ddd++;

        bencHash.start();

        if(algNr == 0){ /// Pirmais algoritms
            hash = hashMat(frame.clone());
            part.push_back((hash + (part.empty()?0:part.back())) / (part.empty()?1:2) );
        }
        if(algNr == 1){ /// Otrais  algoritms
            partMatrix.push_back(parseMat(frame.clone()));
        }
        if(algNr == 2){ /// Otrais+ algoritms
            Mat tMatrix;
            matValues matV;
            int j = -1;

            tMatrix = parseMat(frame.clone());
            matV.x = tMatrix.cols;
            matV.y = tMatrix.rows;
            matV.values = new unsigned char[matV.x*matV.y];

            for(int y = 0;y < matV.y; y++){
                for(int x = 0; x < matV.x; x++){
                    j++;
                    Scalar tValue = tMatrix.at<uchar>(Point(x, y));
                    matV.values[j] = tValue.val[0];
                }
            }

            partMatrix_2.push_back(matV);

        }
        if(algNr == 6){ /// Tresais algoritms

            QList<long> tempHist;
            long tempValue = 0;
            Mat src;
            cvtColor(frame,src,CV_BGR2GRAY);
            uchar * src_p, * src_end;

            long histogram[256] = {0};
            for( src_p = src.data,
                 src_end = src.data + src.rows * src.cols;
                 src_p != src_end; ++src_p)
            {
                histogram[*src_p]++;
            }

            for(int i = 0; i < 256; i++){
                tempValue += histogram[i];
                if(i%int(255/histSize) == 0 && i<250 && i != 0){
                    tempHist.push_back(tempValue);
                    tempValue = 0;
                }
            }
            tempHist.push_back(tempValue);
            partHist.push_back(tempHist);
        }

        hashtime += bencHash.elapsed();
        if(ddd%int(FPS*10) == 0){
            qDebug() << qSetRealNumberPrecision(3) << "    sec:" << int(ddd/FPS) << "=== hash time:" << \
                        hashtime/double(ddd) << "ms ===" << "frame read:"<<ftime/double(ddd)<<"ms";
        }
    }

    qDebug() << " ====================== PART LOADED  ====================== " << bench.elapsed()<<"ms";
}

void Matcher::loadParts()
{
    QTime bench;
    QTime bencHash;
    QTime frameRead;
    long hashtime = 0,ftime = 0;

    Mat frame;
    cap2.open(testVideo.toStdString().c_str());
    if(!cap2.isOpened()){
        qDebug() << "Incorrect test video ===== exiting";
        return;
    }

    if(algNr == 0 && !part.empty())
        part.clear();
    if(algNr == 1 && !partMatrix.empty())
        partMatrix.clear();
    if(algNr == 2 && !partMatrix_2.empty())
        partMatrix_2.clear();
    if(algNr == 6 && !partHist.empty())
        partHist.clear();

    if(FPS == 0)
        setFPS(cap2.get(CV_CAP_PROP_FPS));

    bench.start();
    qDebug() << "";
    qDebug() << " ======================  LOAD PARTS  ====================== ";
    ddd = 0;
    for(;;){
        ddd++;
        frameRead.start();
        cap2 >> frame;
        if(frame.empty())
            break;
        ftime += frameRead.elapsed();
        int hash = 0;
        bencHash.start();

        hash = hashMat(frame.clone());
        part.push_back((hash + (part.empty()?0:part.back())) / (part.empty()?1:2));

        //partMatrix.push_back(parseMat(frame));

        Mat tMatrix;
        matValues matV;
        int j = -1;
        tMatrix = parseMat(frame.clone());
        matV.x = tMatrix.cols;
        matV.y = tMatrix.rows;
        matV.values = new unsigned char[matV.x*matV.y];
        for(int y = 0;y < matV.y; y++){
            for(int x = 0; x < matV.x; x++){
                j++;
                uchar tValue = tMatrix.at<uchar>(Point(x, y));
                matV.values[j] = tValue;
            }
        }
        partMatrix_2.push_back(matV);

        QList<long> tempHist;
        long tempValue = 0;
        Mat src;
        cvtColor(frame,src,CV_BGR2GRAY);
        uchar * src_p, * src_end;

        long histogram[256] = {0};
        for( src_p = src.data,
             src_end = src.data + src.rows * src.cols;
             src_p != src_end; ++src_p)
        {
            histogram[*src_p]++;
        }

        for(int i = 0; i < 256; i++){
            tempValue += histogram[i];
            if(i%int(255/histSize) == 0 && i<250 && i != 0){
                tempHist.push_back(tempValue);
                tempValue = 0;
            }
        }
        tempHist.push_back(tempValue);
        partHist.push_back(tempHist);

        hashtime += bencHash.elapsed();
        if(ddd%int(FPS*10) == 0){
            qDebug() << qSetRealNumberPrecision(3) << "    sec:" << int(ddd/FPS) << "=== hash time:" << \
                        hashtime/double(ddd) << "ms ===" << "frame read:"<<ftime/double(ddd)<<"ms";
        }
    }
    qDebug() << " ====================== PART LOADED  ====================== " << bench.elapsed()<<"ms";
}

        ///======== Otwdvr ==============================================

void Matcher::readClip(QString clipName)
{
    ifBusy = true;
    ifstream fin;
    fin.open(QString(clipName).toStdString().c_str() );
    if(!fin.is_open())
        qDebug() << "Cant open source model Nr 2 =====";
    else{
        if(clipName.endsWith("_matx.txt")){
            partMatrix_2.clear();
            matValues matV;
            int curPos1,curPos2;
            while(true){
                string scValues;
                fin >> scValues;
                if(scValues.empty() || fin.eof()) break;
                curPos1 = scValues.find(":");
                scValues.erase(0,curPos1+1);
                curPos1 = scValues.find(",");
                curPos2 = scValues.find("{");
                matV.x = atoi(scValues.substr(0,curPos1).c_str());
                matV.y = atoi(scValues.substr(curPos1+1,curPos2-1).c_str());
                matV.values = new unsigned char[matV.x*matV.y];
                scValues.erase(0,curPos2+1);

                int j = -1;
                while(curPos1!= -1){
                    ++j;
                    curPos1 = scValues.find(",");
                    if(curPos1 != (int)std::string::npos){
                        matV.values[j] = atoi(scValues.substr(0,curPos1).c_str());
                        scValues.erase(0,curPos1+1);
                    }
                }
                curPos2 = scValues.find("}");
                scValues.erase(curPos2,curPos2+1);
                matV.values[matV.x*matV.y-1] = atoi(scValues.c_str());
                partMatrix_2.push_back(matV);
            }
            fin.close();
        }
        if(clipName.endsWith("_hist.txt")){
            partHist.clear();
            while(true){
                string histValues;
                int curPos1, curPos2;
                fin >> histValues;
                if(histValues.empty() || fin.eof()) break;
                curPos1 = histValues.find(":");
                histValues.erase(0,curPos1+1);
                curPos1 = histValues.find(",");
                curPos2 = histValues.find("{");
                const int size = atoi(histValues.substr(0,curPos2).c_str());
                histValues.erase(0,curPos2+1);

                if(size != histSize){
                    qDebug() << "Different histogram size ===== exiting";
                    return;
                    //setHistSize(size);
                }

                QList<long> hist;
                int j = -1;
                while(curPos1!= -1){
                    j++;
                    curPos1 = histValues.find(",");
                    if(curPos1 != (int)std::string::npos){
                        hist.append(atoi(histValues.substr(0,curPos1).c_str()));
                        histValues.erase(0,curPos1+1);
                    }
                }
                curPos2 = histValues.find("}");
                histValues.erase(curPos2,curPos2+1);
                hist.append(atoi(histValues.c_str()));
                partHist.push_back(hist);
            }
        }
    }
    ifBusy = false;    
}

///=============== Compare parts ========================================

void Matcher::compareParts()
{

    QTime bench;
    QTime bencHash;

//    bool stop = false;
//    int afterStop = 0;
    ddd = 0;
    long hashtime = 0, hashFrames = 0;
    int framenr  = 0, fCount;

    double minDiff = 10, maxDiff = 0;
    fCount = 5;
    delta = 20;
    Results bestResult;
    bestResult.frameNr = -1;
    bestResult.precision = delta*50;

    sMatRes.clear();
    histRes.clear();

    fstream fout;
    //fout.open(QString(sourceVideo+"_results.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);

    if(algNr == 0){ /// Pirmais algoritms

        if(history.empty() || part.empty() || history.size() < part.size())
        {
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;

        }

        qDebug() << " ======================  COMPARING  ======================= ";
        qDebug() << "";
        bench.start();
        for(int i = 0; i < int(history.size() - part.size()); i++)
        {
            double diff = 0;
            int points = 0;

            ddd++;
            bencHash.start();

            for(int y = 0; y < int(part.size()); y++){
                float tmp = abs(history[i+y] - part[y]);
                diff += tmp;
                points ++;

                // Check if we shold continue;
                if(points > fCount && diff/double(points) > 4.19)
                    break;
            }

            diff = diff/(double)points;

            if(diff < minDiff){
                minDiff = diff;
                framenr = i;
            }

            if(points == (int)part.size() && maxDiff < diff){
                maxDiff = diff;
            }

            if(diff <= maxDiff){
                qDebug() << qSetRealNumberPrecision( 2 ) << " === DIFF == H:s" <<
                            QString("%1").arg((int) std::floor((double)i/FPS/60/60), 2).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) ((int)std::floor((double)i/FPS/60)%60) , 2 ).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) std::floor(int(i/FPS)%60 ) , 2).toStdString().c_str()
                             << "  " << diff << " frame "<<i << "cp =" << points;
            }
            hashFrames += bencHash.elapsed();
            if((ddd/FPS/60/60) == int(ddd/FPS/60/60))
                qDebug() << "         ============== HOUR ============== "<<hashFrames/(int(ddd/FPS/60/60))<<"ms";

        }
    }
    if(algNr == 1){ /// Otrais  algoritms

        if(fsMatrix.empty() || partMatrix.empty() || fsMatrix.size() < partMatrix.size())
        {
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }

        bench.start();

        qDebug() << " ======================  COMPARING  ======================= ";
        for(vector<Mat>::iterator it = fsMatrix.begin();it < (fsMatrix.end() - partMatrix.size()); ++it){

            static int deb = 0; deb++;
            float result = 0;
            int points = 0;

            bencHash.start();

            for(int y = 0; y < int(partMatrix.size()); y++){
                points ++;
                result += compareMat(*(it+y),partMatrix[y]);

                // Check if we shold continue;
                if(points > 5 && float(result/points) > 2){
                    break;
                }
            }
            result /= points;
            hashFrames++;
            hashtime += bencHash.elapsed();

            if(result < minDiff){
                minDiff = result;
                framenr = distance(fsMatrix.begin(),it);
            }

            if(result < 1){
                qDebug() << qSetRealNumberPrecision( 2 ) << " === DIFFF == H:s" <<
                            QString("%1").arg((int) std::floor((double)distance(fsMatrix.begin(),it)/FPS/60/60), 2).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) ((int)std::floor((double)distance(fsMatrix.begin(),it)/FPS/60)%60) , 2 ).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) std::floor(int(distance(fsMatrix.begin(),it)/FPS)%60 ) , 2).toStdString().c_str()
                             << "  " << result << " frame "<<distance(fsMatrix.begin(),it) << "cp =" << points;
            }

        }
    }
    if(algNr == 2){ /// Otrais+ algoritms

        if(txtMatrix.empty() || partMatrix_2.empty() || (txtMatrix.size() < partMatrix_2.size()))
        {
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }

        qDebug() << " ======================  COMPARING  ======================= ";        
        bench.start();
        for(int it = 0; it < (distance(txtMatrix.begin(),txtMatrix.end()) - partMatrix_2.size()); ++it){

            ddd++;
            float result = 0;
            int points = 0;           

            bencHash.start();

            for(int y = 0; y < int(partMatrix_2.size()); ++y){

                float tmp = compareMat(txtMatrix[it+y],partMatrix_2[points]);
                points ++;
                result += tmp;
                if(points > fCount && result/double(points) > delta){
                    break;
                }
            }
            result /= points;

            if(result == -1){
                qDebug() << " Not equal mat size ===== exiting";
                return;
            }
            if(result < minDiff){
                minDiff = result;
                framenr = it;
            }

            if(points == partMatrix_2.size()){
                if(maxDiff < result)
                    maxDiff = result;
                if(result < bestResult.precision){
                    bestResult.frameNr = it;
                    bestResult.precision = result;
                }

                qDebug() << qSetRealNumberPrecision( 2 ) << " === DIFF ==" <<
                            QString("%1").arg((int) std::floor((double)it/FPS/60/60), 2).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) ((int)std::floor((double)it/FPS/60)%60) , 2 ).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) std::floor(int(it/FPS)%60 ) , 2).toStdString().c_str()
                             << "  " << result << "== frame"<<it<< "== cp" << points;

                for(int i = 0; i < borders.size(); i++){
                    if(it == borders[i]){
                        bestResult.frameNr = it;
                        bestResult.precision = result/10;
                        break;
                    }
                }
            }
            if(result > delta && bestResult.frameNr != -1){
                sMatRes.append(bestResult);
                bestResult.frameNr = -1;
                bestResult.precision = delta;
            }

            hashtime += bencHash.elapsed();
            if((ddd/FPS/60/60) == int(ddd/FPS/60/60))
                qDebug() << "         ============== HOUR ============== "<<hashtime/(int(ddd/FPS/60/60))<<"ms";
        }
    }
    if(algNr == 3){ /// PSNR    algoritms

        if(fsMatrix.empty() || partMatrix.empty() || fsMatrix.size() < partMatrix.size())
        {
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }

        bench.start();

        qDebug() << " ======================  COMPARING  ======================= ";
        for(vector<Mat>::iterator it = fsMatrix.begin();it < (fsMatrix.end() - partMatrix.size()); ++it){

            ++ddd;
            float result = 0;
            int points = 0;

            bencHash.start();

            for(int y = 0; y < int(partMatrix.size()); y++){
                points ++;
                if((*(it+y)).rows != partMatrix[y].rows || (*(it+y)).cols != partMatrix[y].cols){
                    qDebug() << " not equal mat size ===== exiting";
                    return;
                }

                result += getPSNR(*(it+y),partMatrix[y]);

                // Check if we shold continue;
                if(points > 5 && float(result/points) < 30){
                    break;
                }
            }
            result /= points;
            hashFrames++;
            hashtime += bencHash.elapsed();

            if(result < minDiff){
                minDiff = result;
                framenr = distance(fsMatrix.begin(),it);
            }
            fout << int(result) << std::endl;

            if(result > 30){
                qDebug() << qSetRealNumberPrecision( 2 ) << " === DIFFF == H:s" <<
                            QString("%1").arg((int) std::floor((double)distance(fsMatrix.begin(),it)/FPS/60/60), 2).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) ((int)std::floor((double)distance(fsMatrix.begin(),it)/FPS/60)%60) , 2 ).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) std::floor(int(distance(fsMatrix.begin(),it)/FPS)%60 ) , 2).toStdString().c_str()
                             << "  " << result << " frame "<<distance(fsMatrix.begin(),it) << "cp =" << points;
            }

        }
    }
    if(algNr == 6){ /// Tresais algoritms

        if(txtHist.empty() || partHist.empty() || (txtHist.size() < partHist.size()))
        {
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }

        qDebug() << " ======================  COMPARING  ======================= ";        
        bench.start();

        for(int it = 0; it < int(txtHist.size()-partHist.size()); ++it){

            double diff = 0;
            int points = 0;
            ddd++;

            bencHash.start();

            for(int y = 0; y < int(partHist.size()); y++){
                diff += hashHist(txtHist[it+y],partHist[y]).first;
                points++;
                if(points > fCount && diff/double(points) > delta * 50){
                    break;
                }
            }

            diff /= points;            

            if(diff == -1){
                qDebug() << " Not equal hist size ===== exiting";
                return;
            }
            if(diff < minDiff){
                minDiff = diff;
                framenr = it;
            }
            if(points == (int)partHist.size()){
                if(maxDiff < diff)
                    maxDiff = diff;
                if(diff < bestResult.precision){
                    bestResult.frameNr = it;
                    bestResult.precision = diff;
                }
                qDebug() << qSetRealNumberPrecision( 2 ) << " === DIFF == H:s" <<
                            QString("%1").arg((int) std::floor((double)it/FPS/60/60), 2).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) ((int)std::floor((double)it/FPS/60)%60) , 2 ).toStdString().c_str() << ":" <<
                            QString("%1").arg((int) std::floor(int(it/FPS)%60 ) , 2).toStdString().c_str()
                             << "  " << int(diff) << " frame "<<it << "cp =" << points;

                for(int i = 0; i < borders.size(); i++){
                    if(it == borders[i]){
                        bestResult.frameNr = it;
                        bestResult.precision = diff/10;
                        break;
                    }
                }
            }
            if(diff > delta*50 && bestResult.frameNr != -1){
                histRes.append(bestResult);
                bestResult.frameNr = -1;
                bestResult.precision = delta*50;
            }

            hashtime += bencHash.elapsed();
            if((ddd/FPS/60/60) == int(ddd/FPS/60/60))
                qDebug() << "         ============== HOUR ============== "<<hashtime/(int(ddd/FPS/60/60))<<"ms";
        }
    }

    fout.close();

    qDebug() << "";
    qDebug()  << qSetRealNumberPrecision( 2 ) << " === MIN DIFF" << minDiff << "===== FRAME" << framenr<< "===== HASH"<<hashtime/(double)ddd<<"ms";
    qDebug() << qSetRealNumberPrecision( 2 ) <<" ====================== COMPARING DONE ====================" << bench.elapsed()<<"ms";

}

void Matcher::compareFrame()
{
    Mat gray;
    gray = imread("/media/rihard/videoDisk/abomination.jpg");
    cap.open(sourceVideo.toStdString());

    QPair<long, int> resultPair;
    QTime bench,hashBench;
    double hashTime = 0;

    float result = 0;
    ddd = 0;

    bench.start();

    if(algNr == 0){

        if(history.empty()){
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }

        QString tempName = sourceVideo;
        tempName.chop(4);

        fstream fout;
        fout.open(QString(tempName+"_results_0.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
        if(!fout.is_open())
            qDebug() << " Can't open txt file";


        qDebug() << " ====================== SEARCHING FRAME =================== ";
        qDebug() << "";

        for(int it = 1; it < int(history.size()); ++it){

            result = abs(history[it-1]-history[it]);
            fout << int(result/150*100) << std::endl;

        }
        fout.close();
    }
    if(algNr == 2){

        if(txtMatrix.empty())
        {
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }
        QString tempName = sourceVideo;
        tempName.chop(4);

        fstream fout;
        fout.open(QString(tempName+"_results_2.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
        if(!fout.is_open())
            qDebug() << " Can't open txt file";


        qDebug() << " ====================== SEARCHING FRAME =================== ";
        qDebug() << "";

        QList<int> results;
        bool canSplit = true;
        float avgResult = 0;

        for(int it = 1; it < (distance(txtMatrix.begin(),txtMatrix.end())); ++it){

            int cc = 7;

            result = compareMat(txtMatrix[it-1],txtMatrix[it])*matSize.height*matSize.width;

           if(abs(avgResult-result) > matSize.height*matSize.width*24)
            {
                if(canSplit == true && result > matSize.height*matSize.width*10){
                    qDebug() <<" result ==" <<  result << " avg result ==" << avgResult <<" frame ==" << it;
                    results.clear();
                    canSplit = false;
                }
                else{
                    results.clear();
                    canSplit = true;
                }
            }

            fout << int(result/38250*100) << std::endl;

            avgResult = 0;
            results.push_back(result);
            if(results.size() > cc)
                results.pop_front();
            for(QList<int>::iterator itt = results.begin(); itt < results.end(); ++itt){
                avgResult += *itt;
            }
            avgResult /= results.size();
        }
        fout.close();

    }
    if(algNr == 3){

        if(!cap.isOpened()){
            qDebug() << "video capture not open ===== exiting";
            return;
        }

        QString tempName = sourceVideo;
        tempName.chop(4);

        fstream fout;
        fout.open(QString(tempName+"_results_3.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
        if(!fout.is_open())
            qDebug() << " Can't open txt file";

        qDebug() << " ====================== SEARCHING FRAME =================== ";
        qDebug() << "";

        Mat frame1,frame2;
        cap >> frame1;

        for(;;){
            ddd++;
            if(frame1.empty()){
                cap >> frame1;
                if(frame1.empty())
                    break;

                hashBench.start();
                result = getPSNR(frame1, frame2);
                hashTime += hashBench.elapsed();
                frame2.release();
            }
            else{
                cap >> frame2;
                if(frame2.empty())
                    break;

                hashBench.start();
                result = getPSNR(frame1, frame2);
                hashTime += hashBench.elapsed();
                frame1.release();
            }
            if(50-result < 0)
                fout << 0 << std::endl;
            else
                fout << int((50-result)*2) << std::endl;
        }

        fout.close();

        qDebug() << " Average hash time:" << hashTime/double(ddd) << "ms";

    }
    if(algNr == 4){

        if(!cap.isOpened()){
            qDebug() << "video capture not open ===== exiting";
            return;
        }

        QString tempName = sourceVideo;
        tempName.chop(4);

        fstream fout;
        fout.open(QString(tempName+"_results_4.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
        if(!fout.is_open())
            qDebug() << " Can't open txt file";

        qDebug() << " ====================== SEARCHING FRAME =================== ";
        qDebug() << "";

        Mat frame1,frame2;
        cap >> frame1;

        while(true){
            ddd++;

            Scalar sc;
            if(frame1.empty()){
                cap >> frame1;
                if(frame1.empty())
                    break;

                hashBench.start();
                sc = getMSSIM(frame1,frame2);
                qDebug() <<"R:" << int(sc.val[2]*100) << " G:"<<int(sc.val[1]*100) << " B:" << int(sc.val[0]*100);
                hashTime += hashBench.elapsed();

                frame2.release();
            }
            else{
                cap >> frame2;
                if(frame2.empty())
                    break;

                hashBench.start();
                sc = getMSSIM(frame1,frame2);
                qDebug() <<"R:" << int(sc.val[2]*100) << " G:"<<int(sc.val[1]*100) << " B:" << int(sc.val[0]*100);
                hashTime += hashBench.elapsed();

                frame1.release();
            }

            result = (sc.val[2]+sc.val[1]+sc.val[0])*100/3;
            fout << int(100-result) << std::endl;
        }

        fout.close();

        qDebug() << " Average hash time:" << hashTime/double(ddd) << "ms";



    }
    if(algNr == 5){

        if(!cap.isOpened()){
            qDebug() << "video capture not open ===== exiting";
            return;
        }

        QString tempName = sourceVideo;
        tempName.chop(4);

        fstream fout;
        fout.open(QString(tempName+"_results_5.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
        if(!fout.is_open())
            qDebug() << " Can't open txt file";

        qDebug() << " ====================== SEARCHING FRAME =================== ";
        qDebug() << "";

        Mat frame1,frame2;
        cap >> frame1;

        while(true){
            ++ddd;
            if(frame1.empty()){
                cap >> frame1;
                if(frame1.empty())
                    break;
                hashBench.start();
                result = compHist(frame1.clone(), frame2.clone(), 0);
                hashTime += hashBench.elapsed();
                frame2.release();
            }
            else{
                cap >> frame2;
                if(frame2.empty())
                    break;
                hashBench.start();
                result = compHist(frame1.clone(), frame2.clone(), 0);
                hashTime += hashBench.elapsed();
                frame1.release();
            }

            if(result*100 > 20)
                qDebug() << " diff:"<<result*100<<" == frame:" << ddd << " == Hash time:"<<hashTime/ddd << "ms";
            fout << int(100-result*100) << std::endl;
        }
    }
    if(algNr == 6){

        if(txtHist.empty()){
            qDebug() << "Data isn't loaded correctly ===== exiting";
            return;
        }

        int prevPos = 0;

        QString tempName = sourceVideo;        
        int palka = tempName.lastIndexOf("/");
        tempName.remove(palka,tempName.length());
        tempName.chop(5);

        fstream fout;
        fout.open(QString(tempName+"results_6.txt").toStdString().c_str(),std::fstream::out | std::fstream::trunc);
        if(!fout.is_open())
            qDebug() << " Can't open txt file";

        qDebug() << " ====================== SEARCHING FRAME =================== ";
        qDebug() << "";

        for(int it = 1; it < txtHist.size(); ++it){
            ddd++;
            hashBench.start();            
            resultPair = hashHist(txtHist[it-1],txtHist[it]);
            result = resultPair.first;
            hashTime += hashBench.elapsed();

            if(resultPair.first > 5000 && resultPair.second > 6){
                qDebug() << " result:"<<result<<"hashtime:"<<hashTime/double(ddd);
                if(ddd-prevPos > 1){
                    borders.append(ddd);
                    fout << int(ddd) << std::endl;
                    prevPos = ddd;
                }
            }
        }

        ddd = 0;
        fout.close();
    }

    qDebug() << "";
    qDebug() << " ====================== DONE SEARCHING ==================== " << bench.elapsed() << "ms";
}

///=============== Comparing functions ==================================

int Matcher::hashMat(Mat frame)
{
    int avgim = 0;
    cv::Scalar avgp;

    avgp = cv::mean(frame);
    avgim = ((int)avgp.val[0] + avgp.val[1]+ avgp.val[2]) / 3;

    threshold(frame,frame,avgim,255,THRESH_TOZERO);
    frame = frame - Scalar(avgim,avgim,avgim);

    avgp = cv::mean(frame);

    return  avgp.val[0] + avgp.val[1]+ avgp.val[2];
}

QPair<long, int> Matcher::hashHist(QList<long> hist_1, QList<long> hist_2)
{
    QPair<long, int> result;
    int counter = 0;
    long temp;
    long long diff = 0;

    if(hist_1.size() != hist_2.size()){
        result.first = -1;
        result.second = -1;
        return result;
    }

    for(int i = 0; i < hist_1.size(); i++){
        temp = abs(hist_1[i]-hist_2[i]);
        if(temp > 5000){
            counter++;
        }
        diff += temp;
    }

    result.first = diff/hist_1.size();
    result.second = counter;
    return result;
}

double Matcher::hashOtsu(Mat src)
{
    uchar * src_p, * src_end;
    long histogram[256] = {0};
    long long sum = 0;
    for( src_p = src.data,
         src_end = src.data + src.rows * src.cols;
         src_p != src_end; ++src_p)
    {
        histogram[*src_p]++;
    }
    long total = src.rows * src.cols;
    for(long long i = 0; i < 256; i++) sum += i*histogram[i];
    double sumB = 0;
    double wB = 0;
    double wF = 0;
    double mB;
    double mF;
    double max = 0.0;
    double between = 0.0;
    double threshold1 = 0.0;
    double threshold2 = 0.0;

    for (int i = 0; i < 256; i++) {
        wB += histogram[i];
        if (wB == 0)
            continue;
        wF = total - wB;
        if (wF == 0)
            break;
        sumB += (long long)i * histogram[i];
        mB = (double)sumB / wB;
        mF = (double)(sum - sumB) / wF;
        between = (double)wB * wF * std::pow(mB - mF, 2);
        if ( between >= max ) {
            threshold1 = i;
            if ( between > max ) {
                threshold2 = i;
            }
            max = between;
        }
    }
    return ( threshold1 + threshold2 ) / 2.0;
}

float Matcher::compareMat(Mat frame_1, Mat frame_2)
{

    int diff = 0;
    int pixels = 0;

    if((frame_1.cols != frame_2.cols ) || (frame_1.rows != frame_2.rows)){
        return -1;
    }
    for(int y = 0;y < frame_1.rows; y++){
        for(int x = 0; x < frame_1.cols; x++){
            Scalar f1 = frame_1.at<uchar>(Point(x, y));
            Scalar f2 = frame_2.at<uchar>(Point(x, y));
            diff += abs(f1.val[0] - f2.val[0]);
            ++pixels;
        }
    }
    return (float)(diff/pixels);
}

float Matcher::compareMat(matValues matV1, matValues matV2)
{
    int diff = 0;
    int pixels = 0;
    int j = -1;

    if((matV1.x != matV2.x) || (matV1.y != matV2.y)){
        return -1;
    }
    for(int y = 0; y < matV1.y; y++){
        for(int x = 0; x < matV1.x; x++){
            j++;
            diff += abs(matV1.values[j] - matV2.values[j]);
            ++pixels;
        }
    }
    return (float)(diff/pixels);
}

float Matcher::sortCompare(matValues matV1, matValues matV2)
{
    float result = 0;
    int diff = 0;
    int pixels = 0;
    int j = -1;

    QMultiMap<int,int> tempValues;

    if((matV1.x != matV2.x) || (matV1.y != matV2.y)){
        return -1;
    }

    for(int y = 0; y < matV1.y; y++){
        for(int x = 0; x < matV1.x; x++){
            j++;
            diff += abs(matV1.values[j] - matV2.values[j]);
            ++pixels;
            tempValues.insertMulti(0,diff);
        }
    }

    for(QMultiMap<int,int>::iterator it = tempValues.end(); it != tempValues.begin(); --it){
        result += *it;

    }
    return float(result/pixels);
}

double Matcher::getPSNR(Mat I1, Mat I2)
{
    Mat s1;
    absdiff(I1, I2, s1);       // |I1 - I2|
    s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
    s1 = s1.mul(s1);           // |I1 - I2|^2

    Scalar s = sum(s1);        // sum elements per channel

    double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels

    if( sse <= 1e-10) // for small values return zero
        return 0;
    else
    {
        double mse  = sse / (double)(I1.channels() * I1.total());
        double psnr = 10.0 * log10((255 * 255) / mse);
        return psnr;
    }
}

Scalar Matcher::getMSSIM(Mat i1, Mat i2)
{
 const double C1 = 6.5025, C2 = 58.5225;
 /***************************** INITS **********************************/
 int d     = CV_32F;

 Mat I1, I2;
 i1.convertTo(I1, d);           // cannot calculate on one byte large values
 i2.convertTo(I2, d);

 Mat I2_2   = I2.mul(I2);        // I2^2
 Mat I1_2   = I1.mul(I1);        // I1^2
 Mat I1_I2  = I1.mul(I2);        // I1 * I2

 /***********************PRELIMINARY COMPUTING ******************************/

 Mat mu1, mu2;   //
 GaussianBlur(I1, mu1, Size(11, 11), 1.5);
 GaussianBlur(I2, mu2, Size(11, 11), 1.5);

 Mat mu1_2   =   mu1.mul(mu1);
 Mat mu2_2   =   mu2.mul(mu2);
 Mat mu1_mu2 =   mu1.mul(mu2);

 Mat sigma1_2, sigma2_2, sigma12;

 GaussianBlur(I1_2, sigma1_2, Size(11, 11), 1.5);
 sigma1_2 -= mu1_2;

 GaussianBlur(I2_2, sigma2_2, Size(11, 11), 1.5);
 sigma2_2 -= mu2_2;

 GaussianBlur(I1_I2, sigma12, Size(11, 11), 1.5);
 sigma12 -= mu1_mu2;

 ///////////////////////////////// FORMULA ////////////////////////////////
 Mat t1, t2, t3;

 t1 = 2 * mu1_mu2 + C1;
 t2 = 2 * sigma12 + C2;
 t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

 t1 = mu1_2 + mu2_2 + C1;
 t2 = sigma1_2 + sigma2_2 + C2;
 t1 = t1.mul(t2);               // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

 Mat ssim_map;
 divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

 Scalar mssim = mean( ssim_map ); // mssim = average of ssim map
 return mssim;
}

Mat Matcher::createHist(Mat src)
{
    Mat hsv;

    cvtColor(src, hsv, CV_BGR2HSV);

    // Quantize the hue to 30 levels
    // and the saturation to 32 levels
    int hbins = 30, sbins = 32;
    int histSize[] = {hbins, sbins};
    // hue varies from 0 to 179, see cvtColor
    float hranges[] = { 0, 180 };
    // saturation varies from 0 (black-gray-white) to
    // 255 (pure spectrum color)
    float sranges[] = { 0, 256 };
    const float* ranges[] = { hranges, sranges };
    MatND hist;
    // we compute the histogram from the 0-th and 1-st channels
    int channels[] = {0, 1};

    calcHist( &hsv, 1, channels, Mat(), // do not use mask
             hist, 2, histSize, ranges,
             true, // the histogram is uniform
             false );
    double maxVal=0;
    minMaxLoc(hist, 0, &maxVal, 0, 0);

    int scale = 10;
    Mat histImg = Mat::zeros(sbins*scale, hbins*10, CV_8UC3);

    for( int h = 0; h < hbins; h++ )
        for( int s = 0; s < sbins; s++ )
        {
            float binVal = hist.at<float>(h, s);
            int intensity = cvRound(binVal*255/maxVal);
            rectangle( histImg, Point(h*scale, s*scale),
                        Point( (h+1)*scale - 1, (s+1)*scale - 1),
                        Scalar::all(intensity),
                        CV_FILLED );
        }

    namedWindow( "Source", 1 );
    imshow( "Source", src );

    namedWindow( "H-S Histogram", 1 );
    imshow( "H-S Histogram", histImg );
    waitKey();

    return hsv;
}

double Matcher::compHist(Mat src_base, Mat src_test1, int method)
{
    Mat hsv_base;
    Mat hsv_test1;

    if(src_base.empty() || src_test1.empty())
        qDebug() << "EMPTY FRAME";

    /// Convert to HSV
    cvtColor(src_base, hsv_base, CV_BGR2HSV);
    cvtColor(src_test1, hsv_test1, CV_BGR2HSV);

    /// Using 50 bins for hue and 60 for saturation
    int h_bins = 50; int s_bins = 60;
    int histSize[] = { h_bins, s_bins };

    // hue varies from 0 to 179, saturation from 0 to 255
    float h_ranges[] = { 0, 180 };
    float s_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges, s_ranges };

    // Use the o-th and 1-st channels
    int channels[] = { 0, 1 };


    /// Histograms
    MatND hist_base;
    MatND hist_test1;

    /// Calculate the histograms for the HSV images
    calcHist( &hsv_base, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
    normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );

    calcHist( &hsv_test1, 1, channels, Mat(), hist_test1, 2, histSize, ranges, true, false );
    normalize( hist_test1, hist_test1, 0, 1, NORM_MINMAX, -1, Mat() );

    /// Apply the histogram comparison methods

    //double base_base = compareHist( hist_base, hist_base, method );
    double base_test1 = compareHist( hist_base, hist_test1, method );

    return base_test1;
}

///=============== Mat to matSize(gray) =================================

Mat Matcher::parseMat(Mat frame)
{
    Mat grayFrame (frame.size(), CV_8UC1);
    cvtColor(frame,grayFrame,CV_BGR2GRAY);
    resize(grayFrame,grayFrame,matSize);
    return grayFrame;
}
