INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

FORMS += \
    $$PWD/modelcreator.ui \
    $$PWD/dialog.ui \
    $$PWD/clipfinder.ui

HEADERS += \
    $$PWD/modelcreator.h \
    $$PWD/dialog.h \
    $$PWD/clipfinder.h \
    $$PWD/matcher.h

SOURCES += \
    $$PWD/modelcreator.cpp \
    $$PWD/dialog.cpp \
    $$PWD/clipfinder.cpp \
    $$PWD/matcher.cpp

