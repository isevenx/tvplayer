#include "gotoclass.h"
#include "ui_gotoclass.h"

GoToClass::GoToClass(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GoToClass)
{
    ui->setupUi(this);

    connect(ui->lineEdit,SIGNAL(returnPressed()),this,SLOT(onClick()));
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(onClick()));
    connect(ui->buttonBox,SIGNAL(rejected()),this,SLOT(close()));

}

GoToClass::~GoToClass()
{
    delete ui;
}

void GoToClass::onClick()
{
    sendPosition(ui->lineEdit->text());
    ui->lineEdit->clear();
    this->close();
}
