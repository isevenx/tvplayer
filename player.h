#ifndef PLAYER_H
#define PLAYER_H

#include <QMainWindow>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QThread>
#include <QTime>

#include <fstream>
#include <opencv2/opencv.hpp>

using namespace std;

class Player : public QObject
{
    Q_OBJECT
public:
    explicit Player(QObject *parent = 0);
    ~Player();
    bool ifPlay,ifStarted;
    QString stepFrame;
    QString path;
    QStringList jpgFiles;
    long scrollTo;

public slots:
    void playVideo();
    void stopVideo();
    void setVideo(QString path);

signals:
    void showImage(QPixmap pix, long frameNr);
    void showThumb(QPixmap pix1, QPixmap pix2);

private:
    QImage toImage(cv::Mat inMat);
    int FPS;

};

#endif // PLAYER_H
