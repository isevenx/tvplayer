#include "db.h"
#include <ctime>


DB::DB()
{
    load();
    updated = false;
    counter = -1;
}

DB::~DB()
{

}

/**
 * @brief DataBase::load
 * connects to db
 */
void DB::load()
{
    db = QSqlDatabase::addDatabase("QSQLITE");

    db.setDatabaseName(Settings::getInstance()->settings.value("dbName").toString());
    db.setHostName(    Settings::getInstance()->settings.value("dbUrl").toString());
    db.setUserName(    Settings::getInstance()->settings.value("dbLogin").toString());
    db.setPassword(    Settings::getInstance()->settings.value("dbPassword").toString());


    bool ok = db.open();
    if (ok)
        initTables();
    else
        perror("Error when opening database");
}

/**
 * @brief DataBase::getLastId
 * returns last id
 * @param table table name
 * @return last id
 */
int DB::getLastId(QString table)
{
    try {
        QSqlQuery q;
        q.exec(QString("SELECT MAX(id) from %1").arg(table));
        q.first();
        return q.value(0).toInt();
    }
    catch (const exception &e) {
        cerr << e.what() << endl;
        return -1;
    }
}
/**
 * @brief DB::getConnection
 * not currently in use
 * @return QSqlDatabase
 */
QSqlDatabase DB::getConnection()
{
    return db;
}

/**
 * @brief DB::initTables
 * create table if it is necessary
 */
void DB::initTables()
{
    try {
        QSqlQuery q;
        q = db.exec("CREATE TABLE IF NOT EXISTS frameData("
                    "id           serial primary key,"
                    "name         name(500),"
                    "path         char(500),"
                    "timeStart    DateTime,"
                    "phase        char(10),"
                    "frames       UInt64"
                    ");"
                    );
        if(!q.isActive())
            qDebug() << q.lastError().text();
        else
            qDebug() << "table frameData - OK";
    }
    catch (const exception &e) {
        cerr << e.what() << endl;
    }
}

/**
 * @brief DataBase::insert
 * insert values into DB. Creates new entry.
 * currently not in use
 * @param channelId
 * @param path
 * @param startTime
 * @param duration
 * @param statusId
 * @param createDate
 */
void DB::insert(int channelId, QString path, time_t startTime,
                int duration, int statusId, time_t createDate)
{
    int i = getLastId("video");
    QSqlQuery query;
    query.exec(QString("INSERT INTO video VALUES(%1,'%2','%3',%5,%6,%7)")
                .arg(++i).arg(channelId).arg(path).arg(startTime).arg(duration).arg(statusId).arg(createDate));
}

/**
 * @brief DataBase::save
 * Saves values into db
 * @param name Stream name
 * @param path folder path
 * @param timeStart start time
 * @param phase h for hours - m for minutes;
 * @param frames frame count
 */
void DB::save(QString name,QString path,QDateTime timeStart,QString phase,u_int64_t frames)
{
    int i = getLastId("frameData");
    bool ok;
    QSqlQuery query;
    ok = query.exec(QString("INSERT INTO frameData VALUES(%1,'%2','%3','%4','%5',%6)")
                .arg(++i).arg(name).arg(path).arg(timeStart.toString("yyyy-MM-dd hh:mm:ss")).arg(phase).arg(frames));
    qDebug() <<"Insert into frameData status -"<<ok;
    if (!ok)
        qDebug() << query.lastError().text();

}

/**
 * @brief DB::addData
 * prepare values
 * @param record data for saving
 */
void DB::addData(Record record)
{
    if(!list.empty())
    {
        if (record.path != list.last().path)
            list.append(record);
    }
    else
        list.append(record);
}

/**
 * @brief DB::insert
 * insert prepared values into DB
 * @param timeValue time format
 */
void DB::insert(QString timeValue)
{
    QDateTime date = QDateTime::fromString(getDate(list.at(0).path),"yyyy-MM-dd hh-mm-ss");
    QString path;
    u_int64_t frames = 0;
    if (timeValue=="h")
    {
        QString oldHour = QString(list.at(0).path.at(list.at(0).path.length()-9)) + QString(list.at(0).path.at(list.at(0).path.length()-8));
        for(QList<Record>::iterator it=list.begin();it<list.end();++it)
        {
            QString curHour = QString((*it).path.at((*it).path.length()-9)) + QString((*it).path.at((*it).path.length()-8));
            if (oldHour==curHour)
            {
                frames+=(*it).frameCount;
            }
            else
            {
                path = (*(it-1)).path;
                path.chop(6);
                save((*it).name,path,date,"hours",frames);
                frames = 0;
                frames+=(*it).frameCount;
                oldHour = QString((*it).path.at((*it).path.length()-9)) + QString((*it).path.at((*it).path.length()-8));
                date = QDateTime::fromString(getDate((*it).path),"yyyy-MM-dd hh-mm-ss");
            }
            if(it==list.end()-1)
            {
                path = (*it).path;
                path.chop(6);
                save((*it).name,path,date,"hours",frames);
            }
        }
    }
    if (timeValue=="m")
    {
        QString oldMin = QString(list.at(0).path.at(list.at(0).path.length()-6)) + QString(list.at(0).path.at(list.at(0).path.length()-5));
        for(QList<Record>::iterator it=list.begin();it<list.end();++it)
        {
            QString curMin = QString((*it).path.at((*it).path.length()-6)) + QString((*it).path.at((*it).path.length()-5));
            if (oldMin==curMin)
            {
                frames+=(*it).frameCount;
            }
            else
            {
                path = (*(it-1)).path;
                path.chop(3);
                save((*it).name,path,date,"minutes",frames);
                frames = 0;
                frames+=(*it).frameCount;
                oldMin = QString((*it).path.at((*it).path.length()-6)) + QString((*it).path.at((*it).path.length()-5));
                date = QDateTime::fromString(getDate((*it).path),"yyyy-MM-dd hh-mm-ss");
            }
            if(it==list.end()-1)
            {
                path = (*it).path;
                path.chop(3);
                save((*it).name,path,date,"minutes",frames);
            }
        }
    }
    list.clear();
}

/**
 * @brief DB::getDate
 * prepare time for database
 * @param path full file path
 * @return date in QString format
 */
QString DB::getDate(QString path)
{
    QString date;
    QString tStr = path;
//    qDebug() << path;
    tStr.remove(0,Settings::getInstance()->settings.value("destinationFolder").toString().length());
    if (tStr[0]=='/')
        tStr = tStr.remove(0,1);

    for(int i=0;i<tStr.indexOf('/');i++)
    {
        date += tStr[i];

    }
    tStr.remove(0,tStr.indexOf('/')+1);
    date.replace(QString("_"),QString("-"));
    tStr.chop(1);
    tStr.replace(QString("/"),QString("-"));
    date += " ";
    date += tStr;
    return date;
}

