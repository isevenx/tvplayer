#pragma once

#include <QSettings>

#include <string>
//#include "document.h"

#include <QFile>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonParseError>
#include <QJsonValueRef>
#include <QDateTime>

using namespace std;

class Settings {
private:
    Settings();
    ~Settings();

    static Settings *instance;

    QFile file;
    QString jsonVal;
    QJsonDocument jDoc;
    QJsonObject   jObj;
    QJsonValue    jValue;
    QJsonObject   jItem;

    void setDefaults();

protected:
    static const string fileName;

    void load();

    void loadVideoSource();

    void loadVideoOutput();

    void loadOther();

    void loadDbSet();

public:
    static Settings *getInstance();

    QSettings settings;
};
