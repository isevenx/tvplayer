#ifndef DB_H
#define DB_H

#include <QObject>

#include <QtSql/QSql>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>

#include <QDebug>

#include <settings.h>

#include <iostream>

class DB : public QObject
{
    Q_OBJECT
public:
    //DataBase(QObject *parent = 0);
    DB();
    ~DB();
    /**
     * @brief The Record struct
     * @param path directory
     * @param name stream name
     * @param frameCount frame count in directory
     */
    struct Record{
        QString path;
        QString name;
        u_int64_t frameCount;
    };

    int getLastId(QString table);
    void insert(int channelId, QString path, time_t startTime,
                int duration, int statusId, time_t createDate);
    void save(QString name,QString path,QDateTime timeStart,QString phase,u_int64_t);
    void update(int id,time_t startTime,int duration,time_t createDate);

    void addData(Record);


    QSqlDatabase getConnection();

    static const int RECORDING = 1;
    static const int AVAILABLE = 3;
    static const int ANALYSING = 2;
    static const int ERROR = 4;
    static const int DELETE = 5;
private:

    QSqlDatabase db;

    void initTables();
    QString getDate(QString path);
    void load();

    bool updated;
    int listPos;
    int counter;

    QList<Record> list;
signals:

public slots:
    void insert(QString);
};

#endif // DATABASE_H

