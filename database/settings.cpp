//#include <rapidjson/filereadstream.h>
#include <iostream>
#include "settings.h"
#include <QDebug>

using namespace std;

const string Settings::fileName = "settings.json";   //do not work here  <Qt 5.02>
Settings *Settings::instance = NULL;

Settings::Settings() {
    load();
    loadVideoSource();
    loadVideoOutput();
    loadOther();
    loadDbSet();
}

/**
 * @brief Settings::getInstance
 * @return instance of Settings
 */
Settings *Settings::getInstance() {
    if (!instance) {
        instance = new Settings();
    }
    return instance;
}

/**
 * @brief Settings::load
 * open settings file
 */
void Settings::load() {
    file.setFileName("../otwdvr/settings.json");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        setDefaults();
    jsonVal = file.readAll();
    file.close();
    jDoc = QJsonDocument::fromJson(jsonVal.toUtf8());
    jObj = jDoc.object();
}

/**
 * @brief Settings::loadVideoSource
 * takes video source from json file
 */
void Settings::loadVideoSource() {
    jValue = jObj.value("videoSource");
    jItem  = jValue.toObject();
    settings.setValue("videoSourceUrl",jItem["url"].toString());
}

/**
 * @brief Settings::loadVideoOutput
 * takes output video settings from json file
 */
void Settings::loadVideoOutput() {
    jValue = jObj.value("videoOutput");
    jItem  = jValue.toObject();
    settings.setValue("outputDuration",jItem["duration"].toDouble());
    settings.setValue("outputFps",jItem["fps"].toDouble());               //not in use
    settings.setValue("outputBitRate",jItem["bitRate"].toDouble());       //not in use
    settings.setValue("outputContainer",jItem["container"].toString());   //not in use
    settings.setValue("outputEncoding",jItem["encoding"].toString());     //not in use
}

/**
 * @brief Settings::loadOther
 * takes other settings like file Name,
 * channel Id, destination folder and
 * video Clean interal from json file
 */
void Settings::loadOther() {
    settings.setValue("channleID",jObj.value("channelId").toDouble());                           //not in use
    settings.setValue("destinationFolder",jObj.value("destinationFolder").toString());
    settings.setValue("videoCleanInterval",jObj.value("videoCleanInterval").toDouble());         //not in use
    settings.setValue("fileName",jObj.value("fileName").toString());
}

/**
 * @brief Settings::loadDbSet
 * takes login,password,url and db Name
 * from json file
 */
void Settings::loadDbSet() {
    jValue = jObj.value("database");
    jItem  = jValue.toObject();
    settings.setValue("dbLogin",jItem["login"].toString());                 //not in use
    settings.setValue("dbUrl",jItem["url"].toString());                     //not in use
    settings.setValue("dbPassword",jItem["password"].toString());           //not in use
    settings.setValue("dbName",jItem["dbName"].toString());                 //not in use
}

/**
 * @brief Settings::setDefaults
 * creates JSon file with default settings
 */
void Settings::setDefaults()
{
    file.close();
    qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz ")<<"loading default settings";
    QFile pFile("../otwdvr/settings.json");
    QJsonObject tJObj;
    tJObj.insert("channelId",1);
    tJObj.insert("videoCleanInterval",5);
    tJObj.insert("destinationFolder",QStringLiteral("video/"));
    tJObj.insert("fileName",QStringLiteral("animalPlaneta"));
    QJsonObject tDb;
    tDb.insert("url",QStringLiteral("localhost"));
    tDb.insert("login",QStringLiteral("smarttv"));
    tDb.insert("password",QStringLiteral("smarttv"));
    tDb.insert("dbName",QStringLiteral("smarttv"));
    QJsonObject tVS;
    tVS.insert("url",QStringLiteral("http://iptv.extranet.lv:4040/animal_planet"));
    QJsonObject tDevice;
    tDevice.insert("inputFormat",QStringLiteral("avfoundation"));
    tDevice.insert("deviceName",QStringLiteral("FaceTime HD Camera"));
    tVS.insert("device",tDevice);
    QJsonObject tVO;
    tVO.insert("duration",60);
    tVO.insert("fps",24);
    tVO.insert("bitRate",1024);
    tVO.insert("container",QStringLiteral("mpeg"));
    tVO.insert("encoding",QStringLiteral("mpeg2"));
    tJObj.insert("database",tDb);
    tJObj.insert("videoSource",tVS);
    tJObj.insert("videoOutput",tVO);
    QJsonDocument doc = QJsonDocument(tJObj);
    pFile.open(QIODevice::WriteOnly);
    pFile.write(doc.toJson());
    qWarning() <<QDateTime::currentDateTime().toString("hh:mm:ss:zzz ")<<"Complited";
    file.open(QIODevice::ReadOnly | QIODevice::Text);
}

Settings::~Settings()
{
}
