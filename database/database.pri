INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

HEADERS += \
    $$PWD/db.h\
    $$PWD/settings.h

SOURCES += \
   $$PWD/db.cpp\
   $$PWD/settings.cpp
