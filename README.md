### Main issues ###

* Memory corruption may occur when scrolling ( when writing at the same time with otwdvr? )
* Bad results in clip finding with different image quality ( small matrix algorithm )
* Frame shifting may occur in border finding ( otwdvr or border finding is guilty ? )


### Small issues ###

* Histogram algorithm needs improvement in clip finding ( set threshold the same way as in border finding? )
* Histogram algorithm needs improvement in border finding ( increase threshold for 1 part in order to ignore  frame darkening )
* FPS measurement needs better solution. Right now it only measures time needed to read frame(-s) from disk - no drawing, signals, functions... is involved.

### Algorithm testing ###

* Different matrix size affect
* Different image resolution affect
* Different image quality affect
* **FIND** problematic clips/situations for algorithms

### Algorithm results ###

* ** *Small matrix algorithm* ** 
     * Clip finding
          * Jpg quality change affects results drastically

* ** *Histogram algorithm* **
     * Clip finding
          * Cant find many existing clips
          * Slow frame comparison(~0.0050 ms?)
     * Border finding
          * Problem with darked frames
          * Possible shift in frame identification