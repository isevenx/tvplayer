#include "player.h"

Player::Player(QObject *parent) : QObject(parent)
{
    stepFrame = "";
    path = "";
    ifStarted = false;
    FPS = 25;
}

Player::~Player()
{

}

void Player::setVideo(QString path)
{    
    this->stopVideo();    
    QThread::msleep(150);
    this->path = path;
    QString name = "*.jpg";
    QDirIterator it(path, QStringList() << name, QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()){
        jpgFiles << it.next();
    }
    jpgFiles.sort(Qt::CaseInsensitive);
}

void Player::playVideo()
{
    if(jpgFiles.empty()){
        qDebug() << "Empty image list!";
        return;
    }
    if(ifStarted == true){
        qDebug() << "Already started!";
        this->stopVideo();
        return;
    }
    ifStarted = true;

    QTime parsing;
    QPixmap pixmap;
    QPixmap thumb1,thumb2;

    QStringList::iterator it = jpgFiles.begin();
    while(true){
        if(ifStarted){
            if(ifPlay){
                parsing.start();

                if(stepFrame == ""){
                    if(it-jpgFiles.begin() < jpgFiles.size()-1)
                    ++it;                    
                }
                else{
                    if(stepFrame == "Next"){
                        ++it;
                        ifPlay = false;
                    }
                    if(stepFrame == "Previous"){
                        --it;
                        ifPlay = false;
                    }

                    if(stepFrame.startsWith("Scroll")){
                        while((it-jpgFiles.begin() != scrollTo) && (scrollTo>0) && (scrollTo<jpgFiles.size())){
                            if(it-jpgFiles.begin() < scrollTo){
                                ++it;
                            }
                            if(it-jpgFiles.begin() > scrollTo){
                                --it;
                            }
                        }
                        if(stepFrame == "ScrollPlay"){
                            ifPlay = true;
                        }
                        if(stepFrame == "ScrollStop"){
                            ifPlay = false;
                        }
                    }
                    stepFrame = "";
                }

                if(it-jpgFiles.begin() < 0){
                    while(it != jpgFiles.begin())
                        ++it;
                }
                if(it-jpgFiles.begin() > jpgFiles.size()-2){
                    while(it != jpgFiles.end()-2)
                        --it;
                }


                pixmap.load(*it);
                if(it-jpgFiles.begin()>0){
                    thumb1.load(*(it-1));
                    thumb1 = thumb1.scaled(150,100,Qt::IgnoreAspectRatio, Qt::FastTransformation);
                }
                if(it-jpgFiles.begin() < jpgFiles.size()-1){
                    thumb2.load(*(it+1));
                    thumb2 = thumb2.scaled(150,100,Qt::IgnoreAspectRatio, Qt::FastTransformation);
                }

                if(parsing.elapsed() < 1000/FPS)
                    QThread::msleep((1000/FPS)-parsing.elapsed());

                showImage(pixmap,it-jpgFiles.begin());
                showThumb(thumb1,thumb2);
            }
            else{
                if(stepFrame != "" || ifStarted == false)
                    ifPlay = true;                
                //QThread::msleep(50);
            }
        }
        else break;
    }
}

void Player::stopVideo()
{
    ifStarted = false;
    ifPlay = false;
    stepFrame = "";
    path = "";
    QThread::msleep(100);
    jpgFiles.clear();
}

QImage Player::toImage(cv::Mat inMat)
{

    switch ( inMat.type() )
    {
        // 8-bit, 4 channel
        case CV_8UC4:
        {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32 );
            return image;
        }

        // 8-bit, 3 channel
        case CV_8UC3:
        {
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888 );
            return image.rgbSwapped();
        }

        // 8-bit, 1 channel
        case CV_8UC1:
        {
            static QVector<QRgb>  sColorTable;
            // only create our color table once
            if ( sColorTable.isEmpty() )
            {
               for ( int i = 0; i < 256; ++i )
                  sColorTable.push_back( qRgb( i, i, i ) );
            }
            QImage image( inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8 );
            image.setColorTable( sColorTable );
            return image;
        }

        default:
            qWarning() << "Mat image type not handled in switch:" << inMat.type();
        break;
    }

    return QImage();
}
