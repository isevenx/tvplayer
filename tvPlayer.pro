#-------------------------------------------------
#
# Project created by QtCreator 2015-03-02T11:28:40
#
#-------------------------------------------------

QT       += core sql
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tvPlayer
TEMPLATE = app


INCLUDEPATH += /usr/include
LIBS += -L/usr/lib
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

SOURCES += main.cpp\
        mainplayer.cpp \
    player.cpp \
    gotoclass.cpp

HEADERS  += mainplayer.h \
    player.h \
    gotoclass.h

FORMS    += mainplayer.ui \
    gotoclass.ui

#include (database/database.pri)
include (model/ModelCreator.pri)
