#ifndef MAINPLAYER_H
#define MAINPLAYER_H

#include <QMainWindow>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <opencv2/opencv.hpp>
#include <QThread>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QFile>
#include <QFileDialog>
#include <QMetaType>
#include <QGridLayout>
#include <QGroupBox>
#include <QCheckBox>
#include<QSpacerItem>

#include <iostream>
#include <fstream>

#include "player.h"
#include "modelcreator.h"
#include "gotoclass.h"

namespace Ui {
class MainPlayer;
}

class MainPlayer : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainPlayer(QWidget *parent = 0);
    ~MainPlayer();

    GoToClass *gtc;
    ModelCreator *modelCreator;
    QTime bench;
    struct Mark{
        int start;
        int end;
    };

signals:
    void addMark(Mark);

public slots:
    void browseButton();
    void playButton();
    void nextFrame();
    void previousFrame();
    void scrollTo(int frameNr);    
    void scrollFrame();
    void showImage(QPixmap pix, long frameNr);
    void showThumb(QPixmap pix1, QPixmap pix2);

    void showGoTo();    
    void showFindClips();

    void startMark();
    void endMark();
    void editMark();

    void nextfChange();
    void prevfChange();


private slots:
    void closeEvent(QCloseEvent*);
    void setTime(long frameNr);
    void goTo(QString position);
    void sendJpgFiles(QStringList *jpgFiles);

private:

    QGraphicsScene *scene;
    Player *player;
    QThread *thread;

    Mark mark;
    bool justScroll;
    long currFrame, maxFrame;    
    int sense;
    static const int FPS = 25;
    QList<int> framePositions;

    Ui::MainPlayer *ui;
    QPushButton *prevChange;
    QPushButton *nextChange;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *edit_mark;
    QPushButton *end_mark;
    QPushButton *stark_mark;
    QCheckBox *checkBox;

    QGraphicsView *graphicsView;    

    QSlider *horizontalSlider;
    QSlider *horizontalSlider_2;

    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *next_frame;
    QLabel *prev_frame;
};

#endif // MAINPLAYER_H
