#include "mainplayer.h"
#include "ui_mainplayer.h"

MainPlayer::MainPlayer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainPlayer)
{
    currFrame = 0;
    sense = FPS*7;    

    ui->setupUi(this);
    //  Form creation
    {
    QGridLayout *vBox             = new QGridLayout;
    QGridLayout *buttonsLayout    = new QGridLayout;
    QHBoxLayout *playLayout       = new QHBoxLayout;
    QHBoxLayout *sliderLayout_1   = new QHBoxLayout;
    QHBoxLayout *sliderLayout_2   = new QHBoxLayout;
    QHBoxLayout *editLayout       = new QHBoxLayout;
    QGridLayout *bigLayout        = new QGridLayout;
    QVBoxLayout *buttons          = new QVBoxLayout;
    QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

    playLayout->setSizeConstraint(QLayout::SetMinimumSize);

    label        = new QLabel("Video ieraksts:");

    pushButton   = new QPushButton("Play");
    pushButton_2 = new QPushButton(" < ");
    pushButton_3 = new QPushButton(" > ");
    prevChange   = new QPushButton("<< ");
    nextChange   = new QPushButton(" >>");
    pushButton_4 = new QPushButton("Browse");
    stark_mark   = new QPushButton("Start");
    end_mark     = new QPushButton("End");
    edit_mark    = new QPushButton("Edit Mark");

    checkBox     = new QCheckBox("Stop On");

    label_2      = new QLabel("Frame");
    label_3      = new QLabel("0");
    label_4      = new QLabel("00:00:00");
    label_5      = new QLabel("00:00:00");
    label_6      = new QLabel("Mark:");

    prev_frame   = new QLabel;
    next_frame   = new QLabel;

    prev_frame->setMinimumWidth(150);
    next_frame->setMinimumWidth(150);

    horizontalSlider   = new QSlider(Qt::Horizontal);
    horizontalSlider_2 = new QSlider(Qt::Horizontal);

    editLayout->addWidget(label_6);
    editLayout->addWidget(stark_mark);
    editLayout->addWidget(end_mark);
    editLayout->addWidget(edit_mark);
    editLayout->addSpacerItem(spacer);
    editLayout->addWidget(checkBox);

    buttonsLayout->setColumnStretch(0,1);
    buttonsLayout->addWidget(label,0,0);
    buttonsLayout->addWidget(pushButton_4,0,2);

    sliderLayout_1->addWidget(label_2);
    sliderLayout_1->addWidget(horizontalSlider);
    sliderLayout_1->addWidget(label_3);

    sliderLayout_2->addWidget(label_4);
    sliderLayout_2->addWidget(horizontalSlider_2);
    sliderLayout_2->addWidget(label_5);

    playLayout->addSpacerItem(spacer);
    playLayout->addWidget(prevChange);
    playLayout->addWidget(pushButton_2);
    playLayout->addWidget(pushButton);
    playLayout->addWidget(pushButton_3);
    playLayout->addWidget(nextChange);
    playLayout->addSpacerItem(spacer);

    QGroupBox *gridGroupBox = new QGroupBox;
    QGroupBox *playGroupBox = new QGroupBox;
    QGroupBox *editGroupBox = new QGroupBox;
    QGroupBox *firstSlider  = new QGroupBox;
    QGroupBox *secondSlider = new QGroupBox;
    QGroupBox *mainBox      = new QGroupBox;

    gridGroupBox->setLayout(buttonsLayout);
    playGroupBox->setLayout(playLayout);
    editGroupBox->setLayout(editLayout);

    firstSlider->setLayout(sliderLayout_1);
    secondSlider->setLayout(sliderLayout_2);

    playGroupBox->setContentsMargins(-1, -9, -1, 0);
    firstSlider->setContentsMargins(-1, -8, -1, -1);
    editGroupBox->setContentsMargins(-1, -8, -1, -1);

    buttons->addWidget(playGroupBox);
    buttons->addWidget(firstSlider);
    buttons->addWidget(editGroupBox);

    mainBox->setLayout(buttons);

    prev_frame->setContentsMargins(-1, -10, -1, -10);
    mainBox->setContentsMargins(-1, -10, -1, -10);
    next_frame->setContentsMargins(-1, -10, -1, -10);

    bigLayout->addWidget(prev_frame,0,0);
    bigLayout->addWidget(mainBox,0,1);
    bigLayout->addWidget(next_frame,0,2);

    QGroupBox * box = new QGroupBox;

    box->setLayout(bigLayout);

    graphicsView = new QGraphicsView;
    graphicsView->setContentsMargins(-1, -1, -1, -1);

    gridGroupBox->setContentsMargins(0,0,0,0);
    graphicsView->setContentsMargins(0,0,0,0);
    box->setContentsMargins(0,0,0,0);
    secondSlider->setContentsMargins(0,0,0,0);

    vBox->addWidget(gridGroupBox,0,1);
    vBox->addWidget(graphicsView,1,1);
    vBox->addWidget(box,2,1);
    vBox->addWidget(secondSlider,3,1);

    ui->centralWidget->setLayout(vBox);

    prevChange->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    nextChange->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    pushButton->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    pushButton_2->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    pushButton_3->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    stark_mark->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    edit_mark->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    end_mark->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));

    horizontalSlider_2->setPageStep(50);
    ui->centralWidget->setMinimumHeight(700);
    label_6->setSizePolicy(QSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed));
    }

    edit_mark->setEnabled(false);
    stark_mark->setEnabled(false);
    end_mark->setEnabled(false);

    prevChange->setEnabled(false);
    nextChange->setEnabled(false);

    horizontalSlider->setEnabled(false);

    scene = new QGraphicsScene(this);
    thread = new QThread();
    player = new Player();
    gtc = new GoToClass();
    modelCreator = new ModelCreator();

    player->moveToThread(thread);
    thread->start();

    //  Connect quit function
    //connect(qApp,SIGNAL(aboutToQuit()),this,SLOT(exitProgram()));

    //  Connect buttons
    connect(pushButton_4,SIGNAL(clicked()),this,SLOT(browseButton()));
    connect(pushButton,SIGNAL(clicked()),this,SLOT(playButton()));
    connect(pushButton_2,SIGNAL(clicked()),this,SLOT(previousFrame()));
    connect(pushButton_3,SIGNAL(clicked()),this,SLOT(nextFrame()));
    connect(edit_mark,SIGNAL(clicked()),this,SLOT(editMark()));
    connect(stark_mark,SIGNAL(clicked()),this,SLOT(startMark()));
    connect(end_mark,SIGNAL(clicked()),this,SLOT(endMark()));
    connect(nextChange,SIGNAL(clicked()),this,SLOT(nextfChange()));
    connect(prevChange,SIGNAL(clicked()),this,SLOT(prevfChange()));

    // Connect scrolls
    connect(horizontalSlider,SIGNAL(valueChanged(int)),this,SLOT(scrollTo(int)));
    connect(horizontalSlider_2,SIGNAL(valueChanged(int)),this,SLOT(scrollTo(int)));
    connect(horizontalSlider,SIGNAL(sliderReleased()),this,SLOT(scrollFrame()));
    connect(horizontalSlider_2,SIGNAL(sliderReleased()),SLOT(scrollFrame()));
    connect(pushButton_2,SIGNAL(released()),this,SLOT(scrollFrame()));
    connect(pushButton_3,SIGNAL(released()),this,SLOT(scrollFrame()));
    connect(nextChange,SIGNAL(released()),this,SLOT(scrollFrame()));
    connect(prevChange,SIGNAL(released()),this,SLOT(scrollFrame()));

    //  Connect Player-2-MainPlayer functions
    connect(player,SIGNAL(showImage(QPixmap,long)),this,SLOT(showImage(QPixmap,long)));
    connect(player,SIGNAL(showThumb(QPixmap,QPixmap)),this,SLOT(showThumb(QPixmap,QPixmap)));

    //  Connect menu functions
    connect(ui->actionFind_clips,SIGNAL(triggered()),this,SLOT(showFindClips()));
    connect(ui->actionGo_to,SIGNAL(triggered()),this,SLOT(showGoTo()));
    connect(gtc,SIGNAL(sendPosition(QString)),this,SLOT(goTo(QString)));

    //  Connect MainPlayer-2-ModelCreator function
    connect(this,SIGNAL(addMark(Mark)),modelCreator,SLOT(addMark(Mark)));

    //  Connect ModelCreator-2-Player function
    connect(modelCreator,SIGNAL(getModel(QStringList*)),this,SLOT(sendJpgFiles(QStringList*)));

    //  Connect ClipFinder-2-GoTo function
    connect(modelCreator->clipFinder,SIGNAL(sendGoTo(QString)),this,SLOT(goTo(QString)));
}

MainPlayer::~MainPlayer()
{
    delete ui;
}

void MainPlayer::closeEvent(QCloseEvent*)
{
    player->stopVideo();    
    thread->quit();

    delete gtc;
    delete modelCreator;
}

void MainPlayer::browseButton()
{
    //  Set image folder
    if(pushButton->text() == "Stop")
        pushButton->click();
    QDir dir = QFileDialog::getExistingDirectory(this,
                                         tr("Open Directory"), "/home/rihard/QtPrograms/release-otwdvr/video/",
                                         QFileDialog::ShowDirsOnly
                                         | QFileDialog::DontResolveSymlinks);
    if(dir.dirName().startsWith(".")){
        //qDebug() << "Incorrect folder!";
        return;
    }    

    //  Read image names
    player->setVideo(dir.absolutePath());
    label->setText("Video ieraksts:       "+dir.dirName());

    //  Set Max frame/max time
    int frameCount = player->jpgFiles.size()-1;
    currFrame = 0;
    maxFrame = frameCount;
    int hour,minute,second;
    hour = frameCount/FPS/60/60;
    minute = (frameCount/FPS/60)%60;
    second = (frameCount/FPS)%60;
    QString sTime;
    if(hour < 10)
        sTime.append("0");
    sTime.append(QString::number(hour)+":");
    if(minute < 10)
        sTime.append("0");
    sTime.append(QString::number(minute)+":");
    if(second < 10)
        sTime.append("0");
    sTime.append(QString::number(second));
    label_5->setText(sTime);

    //  Set default scroll values
    justScroll = true;
    horizontalSlider_2->setMaximum(maxFrame);
    horizontalSlider->setMinimum(currFrame-sense);
    horizontalSlider->setMaximum(currFrame+sense);
    horizontalSlider->setValue(currFrame);
    setTime(currFrame);
    justScroll = false;

    //  Clear graphics view
    scene->clear();
    graphicsView->setScene(scene);
    prev_frame->clear();
    next_frame->clear();

    //  Enable mark buttons
    stark_mark->setEnabled(true);
    edit_mark->setEnabled(true);

    //  Set path for clip Finder
    modelCreator->setPath(dir.absolutePath());
    ifstream fin;
    fin.open(QString(dir.absolutePath()+"/results_6.txt").toStdString().c_str());
    if(fin.is_open()){
        while(true){
            if(fin.eof())
                break;
            string tString;
            fin >> tString;
            framePositions.append(QString(tString.c_str()).toInt());
        }
        prevChange->setEnabled(true);
        nextChange->setEnabled(true);
    }
    else{
        qDebug() << "Could not open results!";
    }
    fin.close();

    //  Connect signal to start infinite loop
    connect(pushButton,SIGNAL(clicked()),player,SLOT(playVideo()));
    emit pushButton->click();
}

void MainPlayer::playButton()
{
    //  Disconnect signal until video change
    if(player->ifStarted){
        disconnect(pushButton,SIGNAL(clicked()),player,SLOT(playVideo()));
    }
    //  Change player values and button function
    if(pushButton->text() == "Play"){
        player->ifPlay = true;
        pushButton->setText("Stop");
        horizontalSlider->setEnabled(false);
    }
    else{
        player->ifPlay = false;
        pushButton->setText("Play");
        horizontalSlider->setEnabled(true);
        scrollFrame();
    }
}

void MainPlayer::nextFrame()
{
    //  Changing player class values
    player->stepFrame = "Next";
    if(pushButton->text() == "Stop")
        pushButton->click();
}

void MainPlayer::previousFrame()
{
    //  Changing player class values
    player->stepFrame = "Previous";
    if(pushButton->text() == "Stop")
        pushButton->click();
}

void MainPlayer::scrollTo(int frameNr)
{
    if(justScroll)
        return;

    //  Changing player class values
    if(pushButton->text() == "Stop")
        player->stepFrame = "ScrollPlay";
    else
        player->stepFrame = "ScrollStop";
    player->scrollTo = frameNr;
}

void MainPlayer::scrollFrame()
{
    //  Change value of small scroll bar
    horizontalSlider->setMinimum(currFrame-sense);
    horizontalSlider->setMaximum(currFrame+sense);
    horizontalSlider->setValue(currFrame);
}

void MainPlayer::setTime(long frameNr)
{
    //  Write current time
    horizontalSlider_2->setValue(frameNr);
    int hour,minute,second;
    hour = frameNr/FPS/60/60;
    minute = (frameNr/FPS/60)%60;
    second = (frameNr/FPS)%60;
    QString sTime;
    if(hour < 10)
        sTime.append("0");
    sTime.append(QString::number(hour)+":");
    if(minute < 10)
        sTime.append("0");
    sTime.append(QString::number(minute)+":");
    if(second < 10)
        sTime.append("0");
    sTime.append(QString::number(second));
    label_4->setText(sTime);    
}

void MainPlayer::showImage(QPixmap pix, long frameNr)
{
//    qDebug() << bench.elapsed();
//    bench.start();

    //  Show image to graphics view
    if(frameNr==maxFrame-1){
        if(pushButton->text() == "Stop")
            emit pushButton->click();
    }
    currFrame = frameNr;
    scene->clear();
    scene->addPixmap(pix);
    graphicsView->setScene(scene);
    graphicsView->fitInView(pix.rect());
    label_3->setText(QString::number(frameNr));

    //  Change scroll values without changing frame
    justScroll = true;
    setTime(frameNr);
    justScroll = false;

    //  If stop is checked
    if(checkBox->isChecked()){
        for(QList<int>::iterator it = framePositions.begin(); it < framePositions.end()-1; ++it){
            if(*it == currFrame){
                if(pushButton->text() == "Stop")
                    emit pushButton->click();
            }
        }
    }
}

void MainPlayer::showThumb(QPixmap pix1, QPixmap pix2)
{
    //  Show thumbnail to labels
    prev_frame->clear();
    next_frame->clear();
    prev_frame->setPixmap(pix1);
    next_frame->setPixmap(pix2);
}

void MainPlayer::showGoTo()
{
    //  Show goto form
    if(player->ifStarted){
        gtc->close();
        gtc->show();
    }
    else
        qDebug() << "Player not started!";
}

void MainPlayer::goTo(QString position)
{
    //  Check and go to frame
    int pos = 0;
    if(position.contains(":")){
        qDebug() << position.indexOf(":");
    }
    else{
        QRegExp re("\\d*");
        if(re.exactMatch(position)){
            pos = position.toInt();
            if(pos>-1 && pos<maxFrame){
                currFrame = pos;
                scrollTo(pos);
                scrollFrame();
            }
            else
                qDebug() << "Not existing frame number!";
        }
        else
            qDebug() << "Incorrect position!";
    }
}

void MainPlayer::showFindClips()
{
    if(player->ifStarted){        
        modelCreator->clipFinder->refreshList();
        modelCreator->clipFinder->show();
        if(pushButton->text() == "Stop")
            emit pushButton->click();
    }
    else
       qDebug() << "Player not started!";
}

void MainPlayer::editMark()
{
    modelCreator->close();
    modelCreator->show();
}

void MainPlayer::startMark()
{
    mark.end = 0;
    mark.start = currFrame;
    end_mark->setEnabled(true);
    stark_mark->setEnabled(false);
}

void MainPlayer::endMark()
{
    mark.end = currFrame;
    emit addMark(mark);
    stark_mark->setEnabled(true);
    end_mark->setEnabled(false);
}

void MainPlayer::sendJpgFiles(QStringList *jpgFiles)
{
    if(pushButton->text() == "Stop")
        emit pushButton->click();

    *jpgFiles = player->jpgFiles;
}

void MainPlayer::nextfChange()
{    
    for(QList<int>::iterator it = framePositions.begin(); it < framePositions.end()-1; ++it){
        if(*it > currFrame){
            scrollTo(*(it));
            break;
        }
    }
}

void MainPlayer::prevfChange()
{    
    for(QList<int>::iterator it = framePositions.end()-2; it >= framePositions.begin(); --it){
        if(*it < currFrame){
            scrollTo(*(it));
            break;
        }
    }
}
